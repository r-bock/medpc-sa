\ breakpoint2_L_NF.mpc
\ original: SNS_SABPNPS.mpc
\ Self-administration break point test without delay, no first flush and levers
\ --> for see also SNS_SATNP.mpc, SNS_SASNP.mpc
\ Written by Roland Bock @ NIAAA / NIH
^version = 43
\ - version 4.3 (MAY 2024): changed reward port, because of one broken box
\ - version 4.2 (SEP 2017): added syringe volume check
\ - version 4.1 (OCT 2016): now, displaying total session length instead of remaining time
\ - version 4 (SEP 25, 2015)
\ - version 3.7 (JUNE 28, 2012)
\ - version 0.1 (MAR 23, 2009)
\
\ modification for new set of Med Associates chambers with slightly different
\ port connections and run on a separate computer
\
\ ** Description **
\ This program tests the breakpoint of a self-administering animal. The
\ breakpoint is highest number of lever presses that gave a reward in a progressive ratio.
\ The session either stops after a set time (default 5 hours) or after a certain
\ time after the last reward (default 1 hour).
\ The catheter will NOT be primed (flushed) at the beginning of the session.
\
\ The self-administration happens in the dark. The house light is on before the
\ start of the session and turns back on to signal the end of the session.
\ The ONLY way to turn the house light off at the end of the session is to cut
\ the power to the box or use a macro to turn the house light off.
\
\ This program is based on the training program of this program suite.
\
\ Animals get the same doses of drugs bases on their body weight. The dose is
\ varied by activating the pump for different times. The calculation for the
\ pump time (t) activation is
\
\ t = (body weight * unit injected) / ([drug conc] * pump rate)
\
\ The activation time is calculated in seconds.
\
\ The breakpoint is modeled after Richardson and Roberts, 1996,
\ J Neurosci Meth(66), 1-11, modifying it slightly to offset the inital ratio
\   to the chosen starting ratio.
\
\ The ratios, stored in the array M are calculated by
\      ratio = [5 * exp(infusion number * 0.2)] - 5
\                                                 (rounded to the next integer)
\
\ Currently the test subjects can receive a maximum of 40 rewards.
\
\ ** Error codes **
\ To indicate the non-delivery of a reward, because it failed a check though
\ the animal fullfilled the conditions, 2 arrays, one for the error code, the
\ other for the time stamp are recorded. The codes are
\      1:    no reward because the volume exceeded the volume limit
\      2:    no reward because the dose limit within the time limit was reached
\
\ ** System specific data **
\
\ - catheter volume: 3 �l (micro liter) -> 0.5 sec pump activation to flush
\ - pump rate with 3cc syringe: 6 �l/s (micro-liters per second)
\ - 1 infusion should deliver 1 mg/kg drug for a 25 g mouse
\
\ * Port setup *
\   Output ports
\    1 = left lever (left of the animal side, when facing the wall with the 2 levers)
\    2 = right lever
\    4 = left cue light
\    5 = right cue light
\    7 = house light
\    9 = syringe pump
\   16 = trigger for video
\
\   Input ports
\    1 = left lever
\    2 = right lever
\
\
\ ** Default operation values (which can be changed by the operator) **
\
\ response start ratio: 3
\ length of post-drug time-out: 0 s
\ length of drug session: 1 - 5 hrs
\
\ weight of the animal: 25 g
\ drug concentration: 1.5 mg/ml
\ single injection amount: 1 mg/kg
\ pump injection rate: 6 �l/s
\ overdose control limit: 20 mg/kg within 10 min
\ maximum volume per reward: 80 �l
\
\ right = right when the mouse faces the reward magazine
\ left = left when the mouse faces the reward magazine

\ ****************************
\ CONSTANTS
\ Some of the constants only give an easy access to change the default values.
\ Many of those are used to set some values in the initialization state. Since
\ constants can only be integers, some of the values are 10-fold of their real
\ value. They will be divided by 10 during the initialization.

^InjRate = 6              \ in �l/s (micro-liter per second)
^InjVolLimit = 80         \ maximum injection volume for animal in �l (micro-liter)
^InfusionAmount = 10    \ >> 10 times the real value << in mg/kg x 10
^DrugConcentration = 15 \ >> 10 times the real value << in mg/ml x 10
^StartFR = 3
^DefSyringeVolume = 3	\ default syringe volume (3 ml)

^activeNP = 2         \ input port for active operanda

^timeOut = 0          \ post-drug time-out in seconds
^sessionLength = 5    \ session length in hours
^breakPoint = 1       \ time in hours after last infusion to stop whole session

^videoON = 1
^videoOFF = 0

\ ****************************
\ INPUTS
^right = 2             \ right response operanda input
^left = 1              \ left response operanda input

\ ****************************
\ OUTPUTS
^rightCueLight = 5   \ output port
^leftCueLight = 4    \ output port
^punishLight = 0     \ output port - doesn't currently exist in this setup

^HouseLight = 7
^Reward = 10            \ drug delivery through syringe pump
^Video = 16

\ ****************************
\ DEFINED VARIABLES
\ A = Counter for active responses
\ B = Counter for inactive responses
\ C = Array for timing data of active responses
\ D = Array for timing data of inactive responses
\ E = Array for error codes
\ F = Array for error code timings
\ G = number of failed rewards for visual feedback
\ H = Array for active and inactive inputs and ouputs
\   H(0) = active operanda port number (default is ^right)
\   H(1) = inactive operanda port number
\   H(2) = active cue light
\   H(3)
\   H(4) = program version 

\ I =
\ J = Reward Counter
\ K = Array for reward timing
\ L = Array for lever press ratios for given reward
\ M = Array with the progressive ratios
\ N = Current session time in seconds
\ O = Time limit for pump not to exceed
\ P = Number for index range of injections to reach max limit
\         -> needed to look back into the array to get the sliding time window
\ Q = Array for time calculation of overdose check
\   Q(0) = injection number which would be the limit
\   Q(1) = time difference between Q(0) and current time
\   Q(2) = same as Q(0), just for cont. calc
\   Q(3) = same as Q(1), just for cont. calc
\ R = response counter for visual feedback; counts pokes towards
\         current response ratio.
\ S = Array for timings
\   S(0) = seconds the pump is running (reward)
\   S(1) = post reward time-out in seconds
\   S(2) = drug delivery session length in hours
\   S(3) = break point time in hours
\   S(4) = time variable for infusion (reward)
\   S(5) = time out variable in seconds
\   S(6) = time variable for session
\   S(7) =
\   S(8) = time variable for breakpoint time
\   S(9) = cue light time on in seconds
\   S(10) = time variable for cue light time
\   S(11) = time difference between pump time and cue light
\   S(12) = video sync signal interval

\ T = elapsed time from experiment start in 10 msec increments
\ U = Break point time counter

\ W = Array for animal related measures
\   W(0) = weight of the animal in g
\   W(1) = concentration of the drug (reward) solution in mg/ml
\   W(2) = single unit injection in mg/kg
\   W(3) = pump rate in �l/s (micro-liter per second)
\   W(4) = maximum dose in mg/kg per time frame
\   W(5) = time frame in min for maximum dose
\   W(6) = starting response ratio
\   W(7) = send video signal
\   W(8) = frequency of video signal
\   W(9) = syringe volume in ml
\   W(10) = sum of currently dispensed volume
\   W(11) = single reward volume

\ X = Current ratio of responses
\ Y = Increment for next response ratio
\ Z = previous response ratio -> break point [just for easy access, is also
\                                   recorded in L]

DIM C = 100000     \ time stamps of active responses
DIM D = 100000     \ time stamps of inactive responses
DIM K = 50         \ time stamps of reward deliveries (maximal 40 rewards (length of array M))
DIM E = 10000      \ error code array
DIM F = 10000      \ error timing array
DIM L = 50         \ FR ratio for reward
DIM Q = 3
LIST H = 2, 1, 5, 4, 0                \ inputs of levers, output of cuelights, program version
LIST M = 1, 2, 4, 6, 9, 12, 15, 20, 25, 32, 40, 50, 62, 77, 95, 118, 145, 178, 219, 268, 328, 402, 492, 603, 737, 901, 1102, 1347, 1646, 2012, 2459, 3004, 3670, 4484, 5478, 6692, 8175, 9986, 12198, 14900, 18200
LIST S = 0, 10, 5, 1, 0, 0, 0, 0, 0, 4, 0, 0, 0    \ array for timings (default values)
LIST W = 25, 2, 1, 0, 20, 10, 1, 0, 0, 3, 0, 0  \ array for animal related measures

Var_Alias Weight of animal (g) = W(0)
Var_Alias Drug concentration (mg/ml) = W(1)

\ Var_Alias Infusion time (sec) = S(0)

Var_Alias Post reward time-out (sec) = S(1)
Var_Alias Session length (h) = S(2)
Var_Alias Break point time limit (h) = S(3)
Var_Alias Press start ratio = W(6)
Var_Alias Active lever (2 for right or 1 for left) = H(0)

Var_Alias Single infusion amount (mg/kg) = W(2)
Var_Alias Pumping rate (ul/s) = W(3)
Var_Alias Maximum dose per time (mg/kg) = W(4)
Var_Alias Time for maximum dose (min) = W(5)
Var_Alias Syringe volume = W(9)

Var_Alias Send video sync signal (1 for ON) = W(7)
Var_Alias Frequency of video sync signal (in min) = W(8)

\ PRINTVARS = A, B, C, D, E, F, H, J, K, L, M, O, S, T, W, X, Z
DISKVARS = A, B, C, D, E, F, H, J, K, L, M, O, S, T, W, X, Z

\ ****************************
\ Z-PULSES
\ Z1 = start drug self-administration session
\ Z2 = reward pre-check
\ Z3 = lever press resets timer clock
\ Z4 = stop recording
\ Z5 = start reward delivery
\ Z6 = reward was given
\ Z7 = reward was not given
\ Z8 = pump finished delivering reward

\ *******************************************************
\ ** STATES
\ *******************************************************

\ ****************************
\ INITIALIZATION STATE
S.S.1,
S1,                                    \ set default values
       0.01": SET H(4) = ^version / 10; 		\ record current program version in data
              SET W(6) = ^StartFR;			\ set default start ratio
              SET W(3) = ^InjRate;			\ set default injection rate
              SET H(0) = ^activeNP;             \ set default active response input
              SET S(1) = ^timeOut;			\ set default time-out after reward
              SET S(2) = ^sessionLength;
              SET S(3) = ^breakPoint;
              SET W(1) = ^DrugConcentration / 10;  \ constants can only be integers, so division
              SET W(2) = ^InfusionAmount / 10;     \     by 10 to get to 1.5 as default value
              SET W(7) = ^videoOFF;             \ default: do not send video sync signal
              SET W(8) = (S(2) * 60) + 1;       \ default sync interval: 1 min longer than session length
              SET W(9) = ^DefSyringeVolume;
              SHOW 1,Remaining time (min), 0;
              SHOW 6,Actives, 0;
              SHOW 3,Rewards:, 0;
              SHOW 4,Current FR:, 0;
              SHOW 5,resp. for FR:, R;
              SHOW 2,To break point (min):, 0;
              SHOW 7,Inactives:, 0;
              SET C(A) = -987.987;            \ seal arrays
              SET D(B) = -987.987;
              SET K(J) = -987.987;
              SET E(G) = -987.987;
              SET F(G) = -987.987;
              SET L(J) = -987.987;
              LOCKON ^HouseLight ---> S2         \ start with light on
                                                 \      -> no drug available
S2,
       #START: LOCKOFF ^HouseLight;
               SET X = M(J);
               SET X = X + (W(6) - 1);           \ offset response ratio to start ratio
               SHOW 4, Current FR:, X;
               SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
                                             \ pump activation time in seconds
               SET O = (^InjVolLimit / W(3)) * 1";  \ maximum active pump time
               SET P = (W(4) / W(2));       \ index for overdose control
               SET S(4) = S(0) * 1";        \ infusion time in seconds
               SET S(5) = S(1) * 1";        \ time-out in seconds
               SET S(6) = S(2) * 60 * 1';   \ session length conversion from h to min
               SET S(8) = S(3) * 60 * 1';   \ breakpoint conversion from h to min
               SET S(12) = W(8) * 1';
               IF (S(4) < O) [@VolumeOk, @VolumeBad]
                  @VolumeOk: IF (H(0) = ^right) [@True, @False]
                                 @True:  SET H(1) = ^left;
                                         SET H(2) = ^rightCueLight;
                                         Z1 ---> SX
                                 @False: SET H(1) = ^right;
                                         SET H(2) = ^leftCueLight;
                                         Z1 ---> SX
                  @VolumeBad: SHOW 2, Volume TOO HIGH, 0 ---> STOPKILL
                                               \ problem at the beginning: ABORT

\ ****************************
\ DRUG SELF ADMINISTRATION SESSION
\ After FR X the cue light above the lever comes on and one second
\ later the drug is delivered.
S.S.3,
S1,
       #Z1: LOCKOFF ^HouseLight;
            ON H(0); ON H(1) ---> S2
S2,
       X#RH(0): Z2 ---> S3     \ give reward with Z2 pulse after X on active operanda

S3,
       #Z8: ---> S4        \ pump finished with reward

       #Z7: ---> S7        \ no reward, check for dose

S4,
       S(5)#T: ---> S8     \ honor given time-out

S7,
       1": IF (P <= J) [@CkDose, @OkDose]
               @CkDose:  SET Q(2) = J - P;
                      SET Q(3) = (T - K(Q(2))) / 60;
                      IF (Q(3) > W(5)) [Z9] ---> S8
            @OkDose:  Z9 ---> S8
S8,
       0.01": SET Z = X;
              SET X = M(J);
              SET X = X + (W(6) - 1);
              SHOW 4,Current FR:, X;
              Z9 ---> S2

\ ****************************
\ RESPONSE RECORDING
\ Continous response recording [H(0) is active, H(1) is inactive side]
S.S.4,
S1,
       #START: ---> S2
S2,
       #RH(0): Z3;
               SET C(A) = T;         \ get the time since experiment start
               ADD A;                \ count every lever press
               SET C(A) = -987.987;  \ seal
               SHOW 6, Actives:, A ---> SX
       #RH(1): SET D(B) = T;
               ADD B;
               SET D(B) = -987.987;   \ seal array
               SHOW 7, Inactives:, B ---> SX
       #Z4: ---> S1                    \ stop recording

\ ****************************
\ NOSE POKE RATIO DISPLAY COUNTER
\ This state only provides visual feedback for the experimentator how many
\ lever presses count towards the next lever press ratio.
S.S.10,
S1,
       #START: SET R = 0 ---> S2                \ set counter to zero
S2,
       #RH(0): ADD R;                            \ count lever presses in R
               SHOW 5,resp. for FR:, R ---> SX
       #Z6: SET R = 0; SHOW 5,resp. for FR:, R ---> S3
                                                \ reward given, reset counter
S3,
       #Z9: ---> S2    \ wait for signal to start counting again

\ ****************************
\ VIDEO SYNC SIGNAL CONTROL
\ send out a video disrupt signal (5V) to see start of experiment
S.S.20,
S1,
         #START: IF (W(7) = ^videoON) [ON ^Video] ---> S3
S3,
         0.1": OFF ^Video ---> S4
S4,
         S(12)#T: ON ^Video ---> S3


\ ****************************
\ OVERDOSE AND VOLUME CONTROL
\ Two checks are implemented (s.a.):
\   - the volume can not exceed ^InjVolLimit (original 80 �l) based on the
\     activation time of the pump
\   - the total injected amount over a time frame can not be exceeded.
S.S.25,
S1,
       #Z2: SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
            SET S(4) = S(0) * 1";
            IF (S(4) < O) [@VolumeOk, @VolumeBad]
               @VolumeOk: IF (P <= J) [@CkDose, @OkDose]
                             @CkDose: SET Q(0) = J - P;
                                                       \ look max dose back
                                      SET Q(1) = (T - K(Q(0))) / 60;
                                      \ calculate the time difference from max
                                      \   dose in minutes
                                     IF (Q(1) > W(5)) [@TrueDrug, @FalseNoDrug]
                                        @True: Z5 ---> SX
                                    \ the max dose is outside the time interval
                                        @False: Z7;
                                             SET E(G) = 2, F(G) = T;
                                             ADD G;
                                             SET E(G) = -987.987; \ seal
                                             SET F(G) = -987.987;
                                             SHOW 8, Failed rewards:, G ---> SX
                             @OkDose: Z5 ---> SX
               @VolumeBad: Z7;
                           SET E(G) = 1, F(G) = T;
                           ADD G;
                           SET E(G) = -987.987;        \ seal array
                           SET F(G) = -987.987;
                           SHOW 8, Failed rewards:, G;
                           Z10;
                           LOCKON ^HouseLight ---> STOPABORTFLUSH

\ ****************************
\ SYRINGE VOLUME CHECK
\ Check if the dispensed volume equals or exceeds the set syringe volume. If so, terminate
\ the program, so we don't run an extinction session and know which rewards were actually 
\ delivered.
S.S.26,
S1,
    #START: ---> S2
    
S2,
    #Z5:	SET W(11) = S(0) * W(3);		\ calculate single reward volume
		SET W(10) = W(10) + W(11);	\ maintain running total of dispensed volume
		IF W(10) >= (W(9) * 1000) [Z4] ---> S3		\ request program stop
		
S3,
	0.01": SHOW 1, SYRINGE EMPTY, 0 ---> S1	\ show error message

\ ****************************
\ DRUG DELIVERY
\ Need to have a different state set for delivery to make sure that it doesn't
\ end prematurely when drug session ends
S.S.27,
S1,
       #Z5: Z6;                     \ announce that reward is given
           ON ^Reward;
           SET K(J) = T;        \ get the timestamp of reward
           SET L(J) = X;        \ record the response ratio at reward time
           ADD J;
           SET K(J) = -987.987; \ seal array
           SET L(J) = -987.987;
           SHOW 3, Rewards:, J ---> S2
S2,
       S(4)#T: OFF ^Reward; Z8 ---> S3    \ infusion time for pump
S3,
       #Z9: ---> S1

\ ****************************
\ DRUG AVAILABILITY LIGHT
\ used as an indicator for drug availability for the response operanda
S.S.28,
S1,
       #Z1: ON H(2) ---> S2         \ turn active cue light on
       #Z9: ON H(2) ---> S2
       #Z10: OFF H(2) ---> SX
S2,
       #Z2: OFF H(2) ---> S1

\ ****************************
\ TOTAL SESSION TIMER
\ The session for is at maximum S(2) minutes long.
S.S.29,
S1,
       #START: SET N = 0;            \ reset session timer
               SHOW 1,Remaining time (min):,S(2);
               SHOW 3,Rewards:,J ---> S2
S2,
       1": ADD N;
           SHOW 1,Remaining time (min):, (S(2) * 60) - (N/60);
           IF (N/60 >= S(2)*60) ---> S3    \ S(2) given in hrs, convert in min
      #Z4: ---> S3		\ other state requested stop, process request here
      
S3,                     \ make sure whole session terminates after full finish
       S(4)#T: Z10; LOCKON ^HouseLight; OFF H(0); OFF H(1); OFF H(2) ---> S1

\ ****************************
\ BREAKPOINT TIMER
\ Stops the session after S(7) minutes after the last reward infusion. The Z6
\ pulse is issued after each given reward and resets the timer.
S.S.30,
S1,
        #START: SET U = 0 ---> S2
        #Z6: SET U = 0 ---> S2
S2,
        #Z6: SET U = 0 ---> SX          \ reward given, reset counter
        1": ADD U;
            SHOW 2,To break point (min):, (S(3)*60)-(U/60);
            IF (U/60 >= S(3)*60) [Z10; LOCKON ^HouseLight; OFF H(0); OFF H(1); OFF H(2)] ---> S3
                \ no rewards given, so we can just terminate
S3,
        1': ---> SX
\ ****************************
\ DISPLAY HELPER
\ Just before the program ends, just change the display around for easy user readout
S.S.31,
S1,
        #Z10: CLEAR 6,7;
          SHOW 1, Session time (min):, N/60;
          SHOW 2, Actives:, A;
          SHOW 3, Inactives:, B;
          SHOW 4, Rewards:, J;
          SHOW 5, Breakpoint:, Z;
          SHOW 7, resp. for FR:, R ---> STOPABORTFLUSH

\ ****************************
\ GENERAL TIMER
\ General timer to create a time stamp for frequency calculation in seconds,
\ incremented every 10 ms.
S.S.32,
S1,
       #START: ---> S2
S2,
       0.01": SET T = T + 0.01 ---> SX
