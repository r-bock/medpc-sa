\ DRinSession2_L_NF.mpc
\ original: training2_L_NF v. 4.8
\ Self-administration training with levers and without first flush
\ Written by Roland Bock @ NIAAA / NIH (2016)
\ Date JUNE 7, 2016 version 1
^version = 21
\ - version 2.1 (MAY 2024): changed reward port, because of one broken box
\ - version 2 (SEP 2017): redesigning the program - the first reward will trigger the start
\                         of the first dose, and each dose will have a predefined time and
\                         max number of reward, either of which can end the dose test.
\ - version 1.5 (SEP 2017): changed the trigger to sequence start from first response to
\                           first reward
\ - version 1.4 (AUG 2017): minor bug fixes to ensure the right output ports
\ - version 1.3 (JUL 2017): change that the start of the drug sequence depends on the first
\                 active response.
\ - version 1.2 (JUL 2017): added the option to control 2 syringes with different
\                concentrations to test more doses.
\ - version 1.1 (OCT 2016): added volume monitoring to make sure that all rewards are
\                actually given.
\
\ modification for new set of MedAssociates chambers with slightly different
\ port connections and run on a separate computer
\
\ ** Description **
\ This program implements a dose response test within a single session. Up to 8
\ different doses can be tested each with their individual time periods. Both the
\ doses and their matching time periods can be freely determined and set by the user.
\
\ Setting the time period slot to 0 or the dose to -1 will terminate the sequence. The
\ time slots are initialized to zero, the dose slots to negative one. In this way only
\ the consecutive slots of the sequence need to be filled with the required values.
\
\ The doses will be administered in a consecutive sequence. A maximum number of rewards 
\ per drug dose can also be set to ensure specifically for the lower doses to prevent and
\ extinction response. If the maximum number is reached, the drug period
\ will end and switch to the next one. In this case the comparison value should be the
\ reward rate and not the number of rewards per dose.
\ With a reward limit the drug period can end before the original set time. If the last
\ drug period finishes the program will end.
\
\ If the value of W(13) "change syringe after # doses" is larger than 0 then the syringe
\ port will be switched after the number of periods/doses given in W(13), i.e. if the value
\ is set to 4 the switch to syringe 2 will happen after the 4th dose. If it is kept at
\ the default value of 0 no switch will happen.
\ NOTE: this setup requires that all doses that will be delivered from the same syringe
\ will be kept together in one consecutive block.
\
\ The response ratios will span the dose time boundaries, e.g. if the mouse is on FR3 and
\ presses 2 times before the dose switches and the third will fall in the next dose time,
\ the last press will administer the 2. dose. There is no indication to the mouse if and
\ when the doses change.
\
\ The drug is dispensed through a syringe pump after a fixed number of nose
\ pokes. A post-delivery time out period can be set to prevent the mouse from overdosing.
\ To prevent an accidental overdose a total intake limit within a specified time frame
\ is set, e.g. the default for cocaine is 20 mg/kg within 10 min. The limits can be
\ changed by the user.
\ A cue light above the lever indicates drug availability. It turns of during the pump
\ time necessary to deliver the reward and the set time out period. During this time
\ the presses on the active lever have no consequences.
\
\ (added in version 1.1): The dispensed volume is tracked and compared to the given
\ maximum syringe volume. If the maximum volume is exceed by the tracked volume, the
\ program terminates independent of the other stop conditions. The default syringe volume
\ is 3 milliliter. The tracked volume is in microliter.
\
\ The total number of responses are recorded during the session. The time
\ (in seconds from the beginning of the experiment) of each response is
\ also continuously recorded for later frequency analysis. Similar the total
\ number of rewards per session and their respective time is recorded.
\
\ Animals get the same doses of drugs bases on their body weight. The dose is
\ varied by activating the pump for different times. The calculation for the
\ pump time (t) activation is
\
\ t = (body weight * unit injected) / ([drug conc] * pump rate)
\
\ The activation time is calculated in seconds.
\
\ * Safeguards *
\ Two safeguards are build in. The first should prevent accidental overdose,
\ i.e. the animal has to stay below a certain dose within a previous
\ time frame (default is 20 mg/kg in the previous 10 minutes). If the current
\ dose would get over this limit, no drug is dispensed.
\ The second safeguard is against injecting a two large volume. The limit for
\ this program is 80 �l. If by operator error or other the injected volume is
\ greater than 80 �l per injection, the reward is not given.
\
\ ** Error codes **
\ To indicate the non-delivery of a reward, because it failed a check though
\ the animal fullfilled the conditions, 2 arrays, one for the error code, the
\ other for the time stamp are recorded. The codes are
\      1:    no reward because the volume exceeded the volume limit
\      2:    no reward because the dose limit within the time limit was reached
\
\ ** System specific data **
\
\ - catheter volume: 3 �l (micro liter) -> 0.5 sec pump activation to flush
\ - desired injection (reward) volume: 18 �l (micro liter) for 25 g mouse
\ - pump rate with 3cc syringe: �l/s (micro-liters per second)
\
\ * Port setup *
\   Output ports
\    1 = left lever (left of the animal side, when facing the wall with the 2 levers)
\    2 = right lever
\    4 = left cue light
\    5 = right cue light
\    7 = house light
\    9 = syringe pump
\   16 = trigger for video
\
\   Input ports
\    1 = left lever
\    2 = right lever
\
\ ** Default operation values (which can be changed by the operator) **
\
\ response ratio: 1
\ length of post-drug time-out: 0 s > time out
\ length of drug session: 220 min > sessionLength
\
\ weight of the animal: 25 g
\ drug concentration 1.5 mg/ml
\ single injection amount: 1 mg/kg
\ pump injection rate: 6 �l/s
\ overdose control limit: 20 mg/kg within 10 min
\ maximum volume per reward: 80 �l
\
\ right = right when the mouse faces the reward magazine
\ left = left when the mouse faces the reward magazine
\
\ time the indicator light during the drug dispensation is on: 4 s

\ ****************************
\ CONSTANTS
\ This section is only here to make default value changes more user friendly.
\ Constants can only be integers, so some of the values will be divided by 10
\ in the initialization state

^InjRate = 6          \ in �l/s (micro-liter per second)
^InjVolLimit = 80     \ maximum injection volume for animal in �l (micro-liter)
^DrugConcentration = 15   \ >> 10 times the real value << in mg/ml x 10
^DefSyringeVolume = 3 \ default syringe size

^activeNP = 2         \ default input port for active response

^timeOut = 0              \ post-reward time-out in seconds
\ ^defSessionLength = 360 \ default session length in min (6 h)
^defMaxRewards = 20     \ default maximum number of rewards per dose

\ ****************************
\ DEFINITION CONSTANT
^right = 2             \ right response input
^left = 1              \ left response output

^videoON = 1           \ send video sync signal
^videoOFF = 0          \ do not send video sync signal

\ ****************************
\ OUTPUT PORTS
^rightCueLight = 5     \ output port
^leftCueLight = 4      \ output port
^punishLight = 0       \ output port - but currently not available

^HouseLight = 7
^Reward = 10            \ drug delivery through syringe pump
^Reward_2 = 11         \ second syringe port for different concentration

^Video = 16            \ output port for video sync signal

\ ****************************
\ DEFINED VARIABLES
\ A = Array for active counts per drug period
\ B = Array for inactive counts per drug period
\ C = Array for timing data of active responses
\ D = Array for timing data of inactive responses
\ E = Array for error codes
\ F = Array for error code timings

\ G = number of failed rewards                             -> not recorded

\ H = Array for active and inactive inputs and ouputs
\    H(0) = active response operanda (default is ^right)
\    H(1) = inactive response operanda (default is ^left)
\    H(2) = cue light
\    H(3) = active response operanda output port
\    H(4) = program version
\    H(5) = current reward port

\ I = Array for various indices into arrays                 -> not recorded
\    I(0) = index for active responses
\    I(1) = index for inactive responses
\    I(2) = index for rewards
\    I(3) = index for drug periods
\    I(4) = index into start and end timestamp array for drug periods

\ J = Array for reward counts per drug period
\ K = Array for reward timing

\ L =
\ M =

\ N = Current session time in seconds                      -> not recorded

\ O = time limit for pump not to exceed
\ P = array for start and end timestamps of drug periods

\ Q = array for time calculation of overdose check         -> not recorded
\       Q(0) = injection number which would be the limit
\       Q(1) = time difference between Q(0) and current time
\       Q(2) = same as Q(0), just for cont. calc
\       Q(3) = same as Q(1), just for cont. calc

\ R = array to hold some summations for display            -> not recorded
\    R(0) = total actives count (A)
\    R(1) = total inactives count (B)
\    R(2) = total rewards count (J)

\ S = Array for timings
\    S(0) = seconds the pump is running (reward)
\    S(1) = post reward time-out in seconds
\    S(2) = total session length in minutes
\    S(3) = NOT USED in the training sessions -> set to zero
\    S(4) = time variable for infusion (reward)
\    S(5) = post-reward time out
\    S(6) = NOT USED -> set to zero
\    S(7) = NOT USED -> set to zero
\    S(8) = NOT USED -> set to zero
\    S(9) = NOT USED -> set to zero
\    S(10) = NOT USED -> set to zero
\    S(11) = timing variable for video sync signal frequency
\    S(12) = time variable calculated from the total of the Y array in min

\ T = elapsed time from experiment start in 10 msec increments

\ U = array of position values to display the different counters for the periods
\                                                         |-> not recorded

\ V = array of dosages for each reward (matches in length the K array)

\ W = Array for animal related measures
\         W(0) = weight of the animal in g
\         W(1) = current concentration of the drug (reward) solution in mg/ml
\         W(2) = single unit injection in mg/kg
\         W(3) = pump rate in µl/s (micro-liter per second)
\         W(4) = maximum dose in mg/kg per time frame
\         W(5) = time frame in min for maximum dose
\         W(6) = switch for sending video sync signal or not
\         W(7) = frequency for video signal in min
\         W(8) = single reward volume in microliter
\         W(9) = syringe volume in ml
\         W(10) = sum of currently dispensed volume
\         W(11) = concentration of drug (reward) solution in syringe 1
\         W(12) = concentration of drug (reward) solution in syringe 2
\         W(13) = switch for changing syringes or not
\                    <= 0 -> don't switch syringes
\                    > 0 -> switch to second syringe after I doses/periods
\         W(14) = max number of rewards per drug period

\ X = Ratio of responses

\ Y = array of the individual dose periods in min
\ Z = array of the individual infusion doses in mg/kg per period

DIM I = 4            \ array of indicies
DIM A = 8            \ array for active counts per drug period
DIM B = 8            \ array for inactive counts per drug period
DIM J = 8
DIM C = 10000       \ time stamps of active responses
DIM D = 10000       \ time stamps of inactive response
DIM K = 10000       \ time stamps of reward deliveries
DIM V = 10000       \ administered doses for each reward
DIM E = 10000       \ error code array
DIM F = 10000       \ error timing array
DIM L = 8           \ actives per period counter array
DIM M = 8           \ inactives per period counter array
DIM R = 8           \ rewards per period counter array
DIM Y = 8           \ array of drug period lengths
DIM P = 16          \ array of start and end timestamps of drug periods, 2x the size of Y
DIM Q = 6
LIST H = 2, 1, 5, 4, 0, 0                \ inputs of responses, output of cuelights
LIST S = 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 \ array for timings (default values)
LIST W = 25, 1, 1, 0, 20, 10, 0, 221, 0, 3, 0, 1, 1, 0, 0  \ array for animal related measures
LIST Z = -1, -1, -1, -1, -1, -1, -1, -1, -1  \ array for unit doses in mg/kg for the dose response
LIST U = 0, 7, 8, 9, 10                  \ helper values for display purposes only

Var_Alias Animal weight (g) = W(0)
Var_Alias Max rewards per dose = W(14)
Var_Alias Drug concentration 1 (mg/ml) = W(11)
Var_Alias Drug concentration 2 (mg/ml) = W(12)
Var_Alias Change syringe after # doses = W(13)

Var_Alias Post reward time-out (sec) = S(1)
Var_Alias Lever press ratio = X
Var_Alias Active lever (2 for right or 1 for left) = H(0)

Var_Alias Length of 1. period (min) = Y(0)
Var_Alias Length of 2. period (min) = Y(1)
Var_Alias Length of 3. period (min) = Y(2)
Var_Alias Length of 4. period (min) = Y(3)
Var_Alias Length of 5. period (min) = Y(4)
Var_Alias Length of 6. period (min) = Y(5)
Var_Alias Length of 7. period (min) = Y(6)
Var_Alias Length of 8. period (min) = Y(7)

Var_Alias Dose of 1. period (mg/kg) = Z(0)
Var_Alias Dose of 2. period (mg/kg) = Z(1)
Var_Alias Dose of 3. period (mg/kg) = Z(2)
Var_Alias Dose of 4. period (mg/kg) = Z(3)
Var_Alias Dose of 5. period (mg/kg) = Z(4)
Var_Alias Dose of 6. period (mg/kg) = Z(5)
Var_Alias Dose of 7. period (mg/kg) = Z(6)
Var_Alias Dose of 8. period (mg/kg) = Z(7)

Var_Alias Pumping rate (ul/s) = W(3)
Var_Alias Maximum dose per time (mg/kg) = W(4)
Var_Alias Time for maximum dose (min) = W(5)
Var_Alias Syringe volume = W(9)

Var_Alias Send video sync signal (1 for ON) = W(6)
Var_Alias Frequency of video sync signal (in min) = W(7)

\ define which variables will be saved to disk
DISKVARS = A, B, C, D, E, F, H, J, K, O, P, S, T, V, W, X, Y, Z

\ ****************************
\ Z-PULSES
\ Z1 = start drug self-administration session
\ Z2 = reward pre-check
\ Z4 = stop recording
\ Z5 = start reward delivery
\ Z6 = reward was given
\ Z7 = reward was not given
\ Z8 = pump finished reward delivery
\ Z9 = ready for new reward

\ *******************************************************
\ ** STATES
\ *******************************************************

\ ****************************
\ INITIALIZATION STATE
S.S.1,
S1,                        \ set default values
       0.01": SET H(4) = ^version / 10;         \ constants need to be integers so div 10 > 1.1
              SET X = 1;                        \ default FR 1
              SET W(3) = ^InjRate;              \ default injection rate (pump rate)
              SET H(0) = ^activeNP;             \ default active response port
              SET H(5) = ^Reward;               \ default reward port
              SET S(1) = ^timeOut;              \ default time-out after reward
              SET W(11) = ^DrugConcentration / 10; \ default drug concentration
              SET W(6) = ^videoOFF;             \ default: do NOT sync
              SET W(7) = S(2) + 1;              \ default: send signal only once
              SET Q(4) = 1;                     \ ok flag for reward delivery
              SET Q(5) = 0;                     \ index to check for the time frame limit
              SET W(9) = ^DefSyringeVolume;        \ default: syringe volume 3 ml
              SET W(13) = 0;                    \ no syringe change
              SET W(14) = ^defMaxRewards;       \ default: maximum number of rewards per dose
              SHOW 1,Remain. t. time (min):, 0;
              SHOW 2,Curr. Inf. dose (mg/kg):, 0;
              SHOW 3,Remain. p. time (min), 0;
              SHOW 4,Actives:, 0;
              SHOW 5,Rewards:, 0;
              SET C(I(0)) = -987.987;              \ seal arrays
              SET D(I(1)) = -987.987;
              SET K(I(2)) = -987.987;
              SET V(I(2)) = -987.987;
              SET E(G) = -987.987;
              SET F(G) = -987.987;
              LOCKON ^HouseLight ---> S2         \ indicate to operator that box is loaded
                                                 \      -> no drug available
S2,
       #START: LOCKOFF ^HouseLight;
               SET S(5) = S(1) * 1";                      \ post reward time in seconds
               SET S(11) = W(7) * 1';                     \ timing variable for sync interval in min
               SET O = (^InjVolLimit / W(3)) * 1";        \ maximum active pump time
               IF (H(0) = ^right) [@True, @False]
                    @True:  SET H(2) = ^rightCueLight;    \ active side is right
                            SET H(1) = ^left;
                            Z1 ---> SX                    \ start with Z1 pulse
                    @False: SET H(2) = ^leftCueLight;     \ active side is left
                            SET H(1) = ^right;
                            Z1 ---> SX                    \ start with Z1 pulse

\ ****************************
\ DRUG SELF ADMINISTRATION SESSION
\ After FR X the cue light above the response operanda comes on and one second
\ later the drug is delivered.
S.S.4,
S1,
       #Z1: SHOW 4, Actives:, 0;
            ON H(0);                \ extend the active lever
            ON H(1) ---> S2         \ extend the inactive lever
S2,
       X#RH(0): Z2 ---> S3 \ give reward with Z2 pulse after X responses on active operanda
S3,
       #Z8: ---> S4        \ pump finished reward, now check time-out
       #Z7: ---> S7        \ no reward, check dose before allowing more rewards
S4,
       S(5)#T: Z9 ---> S2  \ honor time-out and be ready again
S7,
       1": IF (Q(4) = 1) [Z9] ---> S2    \ no need to check for volume, bad volume stops

\ ****************************
\ RESPONSE RECORDING
\ Continous response recording [H(0) is active operanda, H(1) the inactive operanda]
S.S.10,
S1,
       #START: ---> S2
S2,
       #RH(0): SET C(I(0)) = T;         \ get the time since experiment start
               ADD I(0);                \ count every response press
               SET C(I(0)) = -987.987;  \ seal
               ADD A(I(3));             \ count actives of this period
               SHOW 4, Actives:, A(I(3)); \ show period actives
               SHOW U(3), Actives:, A(I(3));
               SUMARRAY R(0) = A, 0, 7;
               SHOW 16, T. Actives:, R(0) ---> SX  \ ... and show
       #RH(1): SET D(I(1)) = T;         \ get the time since experiment start
               ADD I(1);                \ count every response
               SET D(I(1)) = -987.987;  \ seal
               ADD B(I(3));             \ count inactives of this period
               SHOW U(2), Inactives:, B(I(3));
               SUMARRAY R(1) = B, 0, 7;
               SHOW 21, T. Inactives:, R(1) ---> SX
       #Z4: ---> S1                    \ stop recording
\       #Z4: OFF H(0); OFF H(1); OFF H(2) ---> S1                    \ stop recording


\ ****************************
\ VIDEO SYNC SIGNAL CONTROL
\ send out a video disrupt signal (5V) to see start of experiment
S.S.20,
S1,
         #START: IF (W(6) = ^videoON) [ON ^Video] ---> S3
S3,
         0.1": OFF ^Video ---> S4
S4,
         S(11)#T: ON ^Video ---> S3


\ ****************************
\ OVERDOSE AND VOLUME CONTROL
\ CAVEAT: this state set has been designed and used for programs in which the
\    reward dose stays static over the whole run. Currently this does not take
\    into account that the dosage is switching and as such can be off when
\    crossing dose change periods -> needs still to be fixed
\ Two checks are implemented (s.a.):
\   - the volume can not exceed ^InjVolLimit (original 30 µl) based on the
\     activation time of the pump
\   - the total injected amount over a time frame can not be exceeded.
S.S.23,
S1,
       #Z2: SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
            SET S(4) = S(0) * 1";        \ convert pump time into MedPC seconds
            IF (S(4) < O) [@VolumeOk, @VolumeBad]
               @VolumeOk: IF (Q(4) = 0) [@NoReward, @OkReward]
                             @NoReward: Z7;   \ reached reward intake limit per time frame
                                        SET E(G) = 2, F(G) = T;
                                        ADD G;
                                        SET E(G) = -987.987; \ seal
                                        SET F(G) = -987.987;
                                        SHOW 5,Failed rewards:, G ---> SX
                             @OkReward: Z5 ---> SX
               @VolumeBad: Z7;
                           SET E(G) = 1, F(G) = T;
                           ADD G;
                           SET E(G) = -987.987;        \ seal array
                           SET F(G) = -987.987;
                           SHOW 1,VOLUME TOO HIGH, 0;
                           SHOW 5,Failed rewards:, G;
                           LOCKON ^HouseLight ---> STOPABORTFLUSH

\ ****************************
\ REWARD TIME MONITOR STATE
\ This state set monitors the reward times and points to the last reward within the
\ limit time frame
S.S.24,
S1,
      #Z5:  SET Q(4) = 1;            \ first reward has been given, no check necessary, set ok flag
            SET Q(5) = 0 ---> S2       \ point to the first reward entry in the array

S2,
      2":   SET Q(1) = (T - K(Q(5))) / 60;    \ set Q1 to time difference in min
            IF (Q(1) > W(5)) [@SeemsOk, @NeedsCheck]
               @SeemsOk:     IF (Q(5) = I(2) - 1) [@Ok, @Adjust]
                                @Ok:      SET Q(4) = 1 ---> SX   \ we are good, give ok
                                @Adjust:  ADD Q(5) ---> SX       \ advance to next time point
               @NeedsCheck:  SUMARRAY Q(6) = V, Q(5), I(2) - 1;
                             IF (Q(6) <= (W(4) - W(2))) [@DoseOk, @DoseBad]
                                @DoseOk:     SET Q(4) = 1 ---> SX \ we are good, give ok
                                @DoseBad:    SET Q(4) = 0 ---> S5 \ overdose limit reached
                                                                      \   set stop flag
     #Z4: SET Q(4) = 0 ---> S10     \ termination signal
S5,
     1": SET Q(1) = (T - K(Q(5))) / 60;          \ check every second if time limit has
         IF (Q(1) > W(5)) [SET Q(4) = 1] ---> S2 \ is over, then continue

     #Z4: SET Q(4) = 0 ---> S10     \ termination signal

S10,
     10': SET Q(4) = 0 ---> SX     \ wait for shutdown

\ ****************************
\ SYRINGE VOLUME CHECK
\ Check for set or default maximum syringe volume to ensure that the program terminates
\ when the syringe is empty
S.S.25,
S1,
      #START: ---> S2
S2,
      #Z5: SET W(8) = S(0) * W(3);      \ get the volume of one reward in microliter
           SET W(10) = W(10) + W(8);    \ add dispensed volume to running total
           IF (W(10) >= (W(9) * 1000)) [Z4] ---> S3       \ syringe volume given in ml
S3,
      0.01": SHOW 1, SYRINGE EMPTY, 0 ---> S1            \ session timer takes care of
                                                  \ termination with Z4

\ ****************************
\ DRUG DELIVERY
\ Need to have a different state set for delivery to make sure that it doesn't
\ end prematurely when drug session ends
S.S.26,
S1,
      #Z5:  Z6;
            ON H(5);
            SET K(I(2)) = T;           \ get time stamp of reward
            SET V(I(2)) = W(2);        \ record reward dose
            ADD I(2);                  \ count reward
            SET K(I(2)) = -987.987;    \ seal array
            SET V(I(2)) = -987.987;
            ADD J(I(3));               \ count period reward
            SHOW 5, Rewards:, J(I(3)); \ show the current rewards
            SHOW U(4), Rewards:, J(I(3));
            SUMARRAY R(2) = J, 0, 7;
            SHOW 11,T. Rewards:, R(2) ---> S2

S2,
     S(4)#T: OFF H(5); Z8 ---> S3
S3,
     #Z9: ---> S1

\ ****************************
\ DRUG AVAILABILITY LIGHT
\ used as an indicator for drug availability for the response operanda
S.S.28,
S1,
       #Z1: ON H(2) ---> S2         \ turn active cue light on
       #Z9: ON H(2) ---> S2
S2,
       #Z2: OFF H(2) ---> S1

\ ****************************
\ DRUG SESSION TIMER
\ This state controls the transition between the different periods and the termination
\ of the program.
S.S.29,
S1,
       #Z1: SET N = 0;                      \ reset session timer
            SET H(5) = ^Reward;             \ set output to syringe 1 port
            SET W(1) = W(11);               \ get first dose from syringe 1
            SET W(2) = Z(I(3));             \ get first dose in mg/kg
            SUMARRAY S(2) = Y, 0, 7;        \ calculate the total session length
            SHOW 1,Remain. t. time (min):, S(2);
            SHOW 2,Curr. Inf. dose (mg/kg):, W(2);
            SHOW 3,Remain. p. time (min):, Y(I(3));
            SHOW 6,Session time (min):, T/60;
            SHOW U(1), Dose (mg/kg):, W(2) ---> S4

S4,
     0.01": SET P(I(4)) = T;                \ record start timestamp
            ADD I(4);
            SET P(I(4)) = -987.987 ---> S5  \ seal array

S5,
       1": ADD N;
           SHOW 1,Remain. t. time (min):, S(2) - (T/60);
           SHOW 3,Remain. p. time (min):, Y(I(3)) - (N/60);
           SHOW 6,Session time (min):, T/60;
           IF (N/60 >= Y(I(3))) ---> S6     \ we are not finished with the period yet

      #Z8: IF (J(I(3)) >= W(14)) ---> S6   \ number of rewards reached set limit, change drug period

S6,
      0.01": SET P(I(4)) = T;          \ record end timestamp
             ADD I(4);
             SET P(I(4)) = -987.987;   \ seal array
             SET N = 0;                \ reset counter for period
             ADD I(3);                 \ increment period/dose index
             SET W(2) = Z(I(3));       \ get next unit dose
             SHOW 2,Curr. Inf. dose (mg/kg):, W(2);            \ update first line for user
             SHOW 3,Remain. p. time (min):, Y(I(3));
             IF (W(13) > 0) AND (I(3) >= W(13)) [@SWITCH, @STAY]
                            @SWITCH:    SET W(1) = W(12);        \ switch concentration to syringe 2 concentration
                                        SET H(5) = ^Reward_2 ---> S8    \ switch to syringe 2 port
                            @STAY: ---> S8

S8,
     0.01": IF (Y(I(3)) > 0) AND (W(2) >= 0) AND (I(3) < 8) [@Cont, @Stop]
                @Cont:  SET U(1) = U(1) + 5;        \ we have another period
                        SET U(2) = U(2) + 5;        \ update display locations
                        SET U(3) = U(3) + 5;
                        SET U(4) = U(4) + 5;
                        SHOW U(1), Dose (mg/kg):, W(2);
                        SHOW U(2), Inactives:, 0;
                        SHOW U(3), Actives:, 0;
                        SHOW u(4), Rewards:, 0 ---> S4    \ continue with new period
                @Stop:  SET J(I(3)) = -987.987;
                        SET A(I(3)) = -987.987;
                        SET B(I(3)) = -987.987;
                        SET Y(I(3)) = -987.987;
                        SET Z(I(3)) = -987.987;
                        Z4 ---> S1            \\ we are done, send stop signal

\ ****************************
\ DISPLAY SWITCHER
\ This set will just switch some values around at the end of the session for easier
\ recording
S.S.30,
S1,
    #Z4: CLEAR 1,5;
         SHOW 2, Session time (min):,T/60;  \ move session time to top row
         SHOW 3, T. Inactives:, I(1);
         SHOW 4, T. Actives:, I(0);
         SHOW 5, T. Rewards:, I(2) ---> SX

\ ****************************
\ SESSION TERMINATION
\ A total session timer is not needed because the entered drug sessions will determine
\ the total length of the session. This state takes care of the termination.
S.S.31,
S1,
    #Z4: ---> S5

S5,
    S(4)#T: LOCKON ^HouseLight ---> STOPABORTFLUSH

\ ****************************
\ GENERAL TIMER
\ General timer to create a time stamp for frequency calculation in seconds,
\ incremented every 10 ms.
S.S.32,
S1,
       #START: ---> S2
S2,
       0.01": SET T = T + 0.01 ---> SX
