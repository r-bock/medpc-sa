\ yoke2_L_NF.mpc
\ Intravenous self-administration yoke procedure
\ --> inspired by SNS_SAY.mpc (first version 2009)
\ Written by Roland Bock @ NIMH / NIH
\ Date Oct 25, 2023, version 0.1
\ * v0.2: Nov 21/2023 added syringe volume check
^version = 21       \ version number times 10
\ - version 2.1 (MAY 2024): changed reward port, because of one broken box
\
\ ** Description **
\ This procedure implements a yoke experiment. This requires the change of the training
\ session procedure to send external K signals to the box running this yoke procedure.
\ The yoking procedure is a control for the voluntary intake of drugs. The yoked animal
\ will experience the same context of the surgery, operant box etc, except that the drug
\ intake is controlled by the animal in the linked box. Lever presses from the yoked mouse
\ are continously recorded, but have no effect on the reward/drug delivery.

\ The K signals are offset by 12, since it is

\ ** Caveats **
\ These programs (both the yoke and linked training/session programs) deliver
\ the reward dose by adjusting the pump time and dispensing the appropriate volume. The
\ pump time is calculated based on the individual mouse weight. This can create an issue
\ when there is a large weight difference between the SA and yoked mouse.
\ When the SA mouse is much lighter, the pump time will be shorter, so it could happen
\ that the ligher SA mouse could already get the next reward, while the pump for the
\ heavier yoked mouse has not finished dispensing the reward. In this case, the heavier
\ mouse would miss the next reward.

\ Should the yoke box run out of reward solution before the paired box (this could happen
\ when the yoked animal is much heavier), the yoked program will stop, without affecting
\ the paired box.

\ ** System specific data **
\
\ - catheter volume: 3 �l (micro liter) -> 0.5 sec pump activation to flush
\ - desired injection (reward) volume: 18 �l (micro liter) for 25 g mouse
\ - pump rate with 3cc syringe: �l/s (micro-liters per second)
\
\ * Port setup *
\   Output ports
\    1 = left lever (left of the animal side, when facing the wall with the 2 levers)
\    2 = right lever
\    4 = left cue light
\    5 = right cue light
\    7 = house light
\    9 = syringe pump
\
\   Input ports
\    1 = left lever
\    2 = right lever


\ ************************
\ CONSTANTS
\ This section lists the constants used in the yoked program. Some of the constants will
\ define default values, which can be changed here quite easily.
\
\ * operational constants *
^InjRate = 6            \ pump rate in �l/s (microliter per seconds)
^InjVolLimit = 80       \ maximum volume for each reward in µl (microliter)
^DefSyringeVolume = 3   \ size of the syringe in the pump for reward delivery.

\ * default values as constants *
^withCue = 1            \ default turn on the cue light for the active lever
^InfusionAmount = 10     \ >> 10 times the real value in mg/kg << i.e. 1 mg/kg
^DrugConcentration = 15 \ >> 10 times the real value in mg/ml << i.e. 1.5 mg/ml in syringe
^activeNP = 2           \ input port for active response

\ ************************
\ DEFINITION CONSTANTS
^right = 2
^left = 1

\ ************************
\ OUTPUT PORTS
^rightCueLight = 5    \ port number for cue light above the right lever
^leftCueLight = 4     \ port number for cue light above the left lever

^HouseLight = 7        \ port for the house light
^Reward = 10           \ port for syringe pump - reward delivery

\ ************************
\ DEFINED VARIABLES
\ A = Counter for active responses
\ B = Counter for inactive responses
\ C = Array for timestamps of active responses
\ D = Array for timestamps of inactive responses

\ E = Array for code of session switch
\ F = Array for timestamps of house light switch
\ G = Counter for session switch

\ H = Array for active and inactive input and output ports
\     H(0) = active responses input port (default is ^right)
\     H(1) = inactive responses input port (default is ^left)
\     H(2) = cue light output port
\     H(3) = active response output port
\     H(4) = program version

\ J = Counter for rewards
\ K = Array for timestamps of rewards

\ O = 

\ T = elapsed time from experiment start in 10 msec increments

\ W = Array for animal related measures
\         W(0) = weight of the animal in g
\         W(1) = concentration of the drug (reward) solution in mg/ml
\         W(2) = single unit injection in mg/kg
\         W(3) = pump rate in ¬µl/s (micro-liter per second)
\         W(4) = maximum dose in mg/kg per time frame
\         W(5) = time frame in min for maximum dose
\         W(6) = not used
\         W(7) = not used
\         W(8) = single reward volume in microliter
\         W(9) = syringe volume in ml
\         W(10) = running total of dispensed volume

\ Z = paired box number

\ *********** ARRAY DEFINITIONS ***********

DIM C = 25000       \ time stamps of active responses
DIM D = 25000       \ time stamps of inactive response
DIM K = 25000       \ time stamps of reward deliveries
DIM E = 20          \ session code switch
DIM F = 20          \ error timing array
LIST H = 2, 1, 5, 4, 0                    \ inputs of responses, output of cuelights
LIST S = 3, 0, 220, 0, 0, 0, 0, 0, 4, 0, 0, 0 \ array for timings (default values)
LIST W = 25, 1, 1.5, 0, 20, 10, 0, 0, 0, 3, 0   \ array for animal related measures


\ ********** USER FRIENDLY VALUE INPUTS ***********
Var_Alias Weight of animal (g) = W(0)
Var_Alias Drug concentration (mg/ml) = W(1)
Var_Alias Pair to box = Z
Var_Alias Active lever (2 for right or 1 for left) = H(0)
Var_Alias Cue lights (0 without or 1 with) = S(11)

Var_Alias Single infusion amount (mg/kg) = W(2)
Var_Alias Pumping rate (ul/s) = W(3)
Var_Alias Maximum dose per time (mg/kg) = W(4)
Var_Alias Time for maximum dose (min) = W(5)
Var_Alias Syringe volume (ml) = W(9)

\ ****** FILE OUTPUT VARIABLES ***********
DISKVARS = A, B, C, D, E, F, G, H, J, K, O, S, T, W, Z

\ ****************************
\ INTERNAL AND EXTERNAL INPUT SIGNALS
\
\ Z-PULSES
\ Z1 = start program after initial checks and side settings.
\ Z2 = reward delivered
\ Z4 = end recording
\ Z5 = "no-drug" period, turn cue light off
\ Z6 = "drug" period, turn cue light on

\ K-PULSES (external to joined box)
\ K pulses are send out by the program of other operant chambers to control this
\    yoked program. BOX here stands for the linked/paired box
\ K BOX + 0 = start the program
\ K BOX + 12 = deliver reward
\ K BOX + 36 = toggle cue light
\ K BOX + 48 = toggle house light
\ K BOX + 60 = end the program
\ K BOX + 72 = error in the linked program - stop here

\ *******************************************************
\ ** STATES
\ *******************************************************

\ ****************************
\ INITIALIZATION STATE
S.S.1,
S1,                        \ set default values
       0.01": SET H(4) = ^version / 10;         \ record current program version in data
              SET W(3) = ^InjRate;              \ default injection rate (pump rate)
              SET H(0) = ^activeNP;             \ default active response port
              SET W(1) = ^DrugConcentration / 10; \ constants can only be integers, so division
              SET W(2) = ^InfusionAmount / 10;  \     by 10 to get to 1.5
              SET W(9) = ^DefSyringeVolume;     \ default: syringe volume in ml (3)
              SET S(11) = ^withCue;             \ default: turn cue lights on
              SET Z = 0;                        \ = empty -> user needs to set linked box, no default
              SHOW 1,paired to:, Z;
              SHOW 2,Actives:, 0;
              SHOW 3,Inactives:, 0;
              SHOW 4,Rewards:, 0;
              SET C(A) = -987.987;            \ seal arrays
              SET D(B) = -987.987;
              SET K(J) = -987.987;
              SET E(G) = -987.987;
              SET F(G) = -987.987;
              LOCKON ^HouseLight ---> S2         \ indicate to operator that box is loaded
                                                 \      -> no drug available

S2,
       #START: IF (BOX = Z) OR (Z = 0) [@STOP, @CONTINUE]
                  @STOP:     SHOW 1, PAIRING ERROR:, Z ---> STOPKILL
                                             \ pairing either not done or paired to same box
                  @CONTINUE: SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
                                             \ pump activation time in seconds
                             SET O = (^InjVolLimit / W(3)) * 1"; \ maximum active pump time
                             SHOW 1,paired to:, Z;
                             SET S(4) = S(0) * 1";   \ infusion time in seconds
                             IF (S(4) < O) [@VolumeOk, @VolumeBad]
                                @VolumeOk:  IF (H(0) = ^right) [@Right, @Left]     \ set active sides
                                               @Right:   SET H(2) = ^rightCueLight;
                                                         SET H(1) = ^left ---> S3
                                               @Left:    SET H(2) = ^leftCueLight;
                                                         SET H(1) = ^right ---> S3
                                @VolumeBad: SHOW 2, Volume TOO HIGH, 0 ---> STOPKILL \ problem -> ABORT

S3,
       #K(Z): LOCKOFF ^HouseLight; Z1 ---> SX    \ wait for the paired box to start the program
       #K(Z + 72): LOCKON ^HouseLight;
                   SHOW 1, Linked Box STOPPED!, Z ---> STOPKILL \ the linked box aborted, stop also


\ ****************************
\ RESPONSE RECORDING
\ Continuous response recording [H(0) is active operanda, H(1) the inactive operanda]
\ In this yoked procedure the levers have no effect on drug delivery, but the responses
\ are recorded.
S.S.4,
S1,
       #Z1: ---> S2
S2,
       #RH(0): SET C(A) = T;         \ get the time since experiment start
               ADD A;                \ count every response press
               SET C(A) = -987.987;   \ seal
               SHOW 2, Actives:, A ---> SX  \ ... and show
       #RH(1): SET D(B) = T;         \ get the time since experiment start
               ADD B;                \ count every response
               SET D(B) = -987.987;   \ seal
               SHOW 3, Inactives:, B ---> SX
       #K(Z + 60): ---> S1                    \ stop recording
       #Z4: ---> S1                           \ stop recording (internal error)


\ ****************************
\ SYRINGE VOLUME CHECK
\ Check the totally dispense volume against the total syringe volume to be able to
\ terminate the program when the drug solution is empty.
S.S.23,
S1,
    #Z1: ---> S2

S2,
    #Z2: SET W(8) = S(0) * W(3);          \ calculate reward volume in microliter
         SET W(10) = W(10) + W(8);        \ keep running total
         IF (W(10) >= (W(9) * 1000)) [Z4] ---> S3
S3,
    0.01": SHOW 5, SYRINGE EMPTY, T/60 ---> S1

\ ****************************
\ DRUG DELIVERY
\ Need to have a different state set for delivery to make sure that it doesn't
\ end prematurely when drug session ends
S.S.26,
S1,
      #K(Z + 12): Z2;                     \ turn cue light off
                  ON ^Reward;
                  SET K(J) = T;           \ get time stamp of reward
                  ADD J;                  \ count reward
                  SET K(J) = -987.987;    \ seal array
                  SHOW 4,Rewards:, J ---> S2

S2,
     S(4)#T: OFF ^Reward ---> S1

\ ****************************
\ DRUG AVAILABILITY LIGHT
\ used as an indicator for drug availability for the response operanda
S.S.28,
S1,
       #Z1: IF (S(11) = ^withCue) [ON H(2)] ---> S5
S2,
       #K(Z + 36): ON H(2) ---> S5
       #Z6: ON H(2) ---> S5         \ drug period resumes
S5,
       #Z2: OFF H(2) ---> S2        \ reward is delivered
       #Z5: OFF H(2) ---> S2        \ "no-drug" period
       #K(Z + 36): OFF H(2) ---> S2

\ ****************************
\ SESSION TIMER / CONTROL STATE
\ The yoked session is controlled by the paired box (paired box given in Z).
S.S.29,
S1,
    #Z1: SHOW 1,connected to:, Z;
         SHOW 4,Rewards:, J;
         ON H(0); ON H(1) ---> S2

S2,
    #K(Z + 48): Z5;
                LOCKON ^HouseLight;  \ we have a "drug-off" period
                SET F(G) = T;        \ record the time of it.
                SET E(G) = 1;
                ADD G;
                SET F(G) = -987.987;
                SET E(g) = -987.987 ---> S3
    #K(Z + 60): ---> S4    \ session ended in paired box
    #Z4: ---> S4           \ this box ended session

S3,
    #K(Z + 48): LOCKOFF ^HouseLight;
                SET F(G) = T;
                SET E(G) = 0;
                ADD G;
                SET F(G) = -987.987;
                SET E(G) = -987.987 ---> S2
    #K(Z + 60): ---> S4    \ session ended in paired box
    #Z4: ---> S4           \ this box ended session

S4,
    S(4)#T: LOCKON ^HouseLight; OFF H(2); OFF H(0); OFF H(1) ---> STOPABORTFLUSH

\ ****************************
\ GENERAL TIMER
\ General timer to create a time stamp for frequency calculation in seconds,
\ incremented every 10 ms. It gets started by the connected box with K[1-12].
S.S.32,
S1,
       #Z1: ---> S2
S2,
       0.01": SET T = T + 0.01 ---> SX
