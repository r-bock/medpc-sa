\ training.mpc
\ original: SNS_SATNPS.mpc
\ Self-administration training without pump delay
\ --> for see also sessions.mpc, breakpoint.mpc and punishment.mpc
\ Written by Roland Bock @ NIAAA / NIH (2008)
\ Date SEPTEMBER 3, 2011 version 4.1

\ ** Description **
\ This program implements the training sessions for the self-administration
\ schedule. This is a variation of the SNS_SAT.mpc program. The 
\ program implements only one session with drug availability. Contrary to the 
\ original program, the training session is done in the dark, i.e. the house 
\ light is off for the self-administration. The ONLY way to turn the house light 
\ off after the training session is to cut the power to the box or run a macro 
\ to turn it off.
\
\ The drug is dispensed through a syringe pump after a fixed number of nose 
\ pokes. A post-delivery time-out period prevents the animal from overdosing. 
\ A cue light opposite the nose poke holes indicates drug availability.
\
\ The total number of nose pokes are recorded during the session. The time 
\ (in seconds from the beginning of the experiment) of each nose poke is 
\ also continuously recorded for later frequency analysis. Similar the total 
\ number of rewards per session and their respective time is recorded.
\
\ The light in the active nose poke hole turns on for 4 seconds as cue light and  
\ the pump starts delivering the drug.
\
\ With the START of the experiment the catheter of the animal is flushed with a 
\ single drug dose, which is not counted as a reward.
\
\ Animals get the same doses of drugs bases on their body weight. The dose is 
\ varied by activating the pump for different times. The calculation for the 
\ pump time (t) activation is 
\
\ t = (body weight * unit injected) / ([drug conc] * pump rate)
\
\ The activation time is calculated in seconds.
\
\ Two safeguards are build in. The first should prevent accidental overdose, 
\ i.e. the animal has to stay below a certain dose within a previous
\ time frame (default is 20 mg/kg in the previous 10 minutes). If the current
\ dose would get over this limit, no drug is dispensed.
\ The second safeguard is against injecting a two large volume. The limit for 
\ this program is 80 µl. If by operator error or other the injected volume is
\ greater than 80 µl per injection, the reward is not given.
\
\
\ ** Error codes **
\ To indicate the non-delivery of a reward, because it failed a check though 
\ the animal fullfilled the conditions, 2 arrays, one for the error code, the 
\ other for the time stamp are recorded. The codes are 
\      1:    no reward because the volume exceeded the volume limit
\      2:    no reward because the dose limit within the time limit was reached
\
\ ** System specific data **
\
\ - catheter volume: 3 �l (micro liter) -> 0.5 sec pump activation to flush
\ - desired injection (reward) volume: 18 �l (micro liter) for 33g mouse
\ - pump rate with 3cc syringe: �l/s (micro-liters per second)
\ - 1 infusion should deliver 1 mg/kg drug for a 33 g mouse [1.833 mg/ml drug]
\ --> (33 g * 1 mg/kg) / (1.8333 mg/ml * 6 �l/s) = 3 seconds pump activation
\ 
\ ** Default operation values (which can be changed by the operator) **
\
\ nose poke ratio: 1
\ length of post-drug time-out: 0 s > time out
\ length of drug session: 220 min > sessionLength
\
\ weight of the animal: 33 g
\ drug concentration 1.5 mg/ml
\ single injection amount: 1 mg/kg   (half of amount in actual sessions)
\ pump injection rate: 6 �l/s
\ overdose control limit: 20 mg/kg within 10 min
\ maximum volume per reward: 80 �l
\
\ right = right when the mouse faces the reward magazine
\ left = left when the mouse faces the reward magazine
\ 
\ time the indicator light during the drug dispensation is on: 4 s

\ ****************************
\ CONSTANTS
\ This section is only here to make default value changes more user friendly.
\ Constants can only be integers, so some of the values will be divided by 10
\ in the initialization state

^InjRate = 6          \ in �l/s (micro-liter per second)
^InjVolLimit = 80     \ maximum injection volume for animal in �l (micro-liter)
^InfusionAmount = 10      \ >> 10 times the real value << in mg/kg x 10
^DrugConcentration = 15   \ >> 10 times the real value << in mg/ml x 10

^activeNP = 7         \ input port for active nose poke
^outPortOffset = 6    \ offset of output port from input port for holes

^timeOut = 0          \ post-reward time-out in seconds 
^sessionLength = 220  \ session length in minutes
^NPlightOn = 4        \ 4 seconds to keep the light on

\ ****************************
\ DEFINITION CONSTANT
^right = 7             \ right nose poke input
^left = 8              \ left nose poke output

\ ****************************
\ OUTPUT PORTS
^rightCueLight = 10   \ output port
^leftCueLight = 11    \ output port
^punishLight = 12     \ output port

^HouseLight = 15
^Reward = 9            \ drug delivery through syringe pump

\ ****************************
\ DEFINED VARIABLES
\ A = Counter for active nose pokes
\ B = Counter for inactive nose pokes
\ C = Array for timing data of active nose pokes
\ D = Array for timing data of inactive nose pokes
\ E = Array for error codes
\ F = Array for error code timings

\ G = number of failed rewards

\ H = Array for active and inactive inputs and ouputs
\	H(0) = active nose poke hole (default is ^right)
\	H(1) = inactive nose poke hole (default is ^left)
\	H(2) = cue light
\       H(3) = active nose poke hole output port

\ I = Index array for nose pokes counter array
\       I(0) = for active nose poke timing data
\       I(1) = for inactive nose poke timing data
\       I(2) = for reward timing data
\       I(3) = for error codes

\ J = Reward Counter
\ K = Array for reward timing

\ N = Current session time in seconds

\ O = time limit for pump not to exceed
\ P = number for index range of injections to reach max limit
\     -> needed to look back into the array to get the sliding time window

\ Q = array for time calculation of overdose check
\       Q(0) = injection number which would be the limit
\       Q(1) = time difference between Q(0) and current time
\       Q(2) = same as Q(0), just for cont. calc
\       Q(3) = same as Q(1), just for cont. calc

\ S = Array for timings
\	S(0) = seconds the pump is running (reward)
\	S(1) = post reward time-out in seconds
\	S(2) = drug delivery session length in minutes
\	S(3) = NOT USED in the training sessions -> set to zero
\	S(4) = time variable for infusion (reward)
\	S(5) = NOT USED -> set to zero
\	S(6) = time variable for drug delivery session
\       S(7) = NOT USED -> set to zero
\       S(8) = time the nose poke light will be on after successful poke
\       S(9) = timing variable for nose poke light
\       S(10) = timing variable for the difference between S(9) and S(4) to ensure
\               that the cue light and the reward delivery are in sync.


\ T = elapsed time from experiment start in 10 msec increments

\ W = Array for animal related measures
\         W(0) = weight of the animal in g
\         W(1) = concentration of the drug (reward) solution in mg/ml
\         W(2) = single unit injection in mg/kg
\         W(3) = pump rate in µl/s (micro-liter per second)
\         W(4) = maximum dose in mg/kg per time frame
\         W(5) = time frame in min for maximum dose

\ X = Ratio of nose pokes

DIM I = 3           \ array of indices for various arrays (ran out of names)
DIM C = 10000       \ time stamps of active nose pokes
DIM D = 10000       \ time stamps of inactive nose poke
DIM K = 10000       \ time stamps of reward deliveries
DIM E = 10000       \ error code array
DIM F = 10000       \ error timing array
DIM Q = 3
LIST H = 7, 8, 0, 13                      \ inputs of nose pokes, output of cuelights
LIST S = 3, 0, 220, 0, 0, 0, 0, 0, 4, 0, 0 \ array for timings (default values)
LIST W = 33, 1, 1, 0, 20, 10  \ array for animal related measures

Var_Alias Weight of animal (g) = W(0)
Var_Alias Drug concentration (mg/ml) = W(1)

\ Var_Alias Infusion time (sec) = S(0)

Var_Alias Post reward time-out (sec) = S(1)
Var_Alias Session length (min) = S(2)
Var_Alias Poke ratio = X
Var_Alias Active hole (7 for right or 8 for left) = H(0)

Var_Alias Single infusion amount (mg/kg) = W(2)
Var_Alias Pumping rate (�l/s) = W(3)
Var_Alias Maximum dose per time (mg/kg) = W(4)
Var_Alias Time for maximum dose (min) = W(5)

\ PRINTVARS = A, B, C, D, E, F, H, J, K, O, S, T, W, X
DISKVARS = A, B, C, D, E, F, H, J, K, O, S, T, W, X

\ ****************************
\ Z-PULSES
\ Z1 = start drug self-administration session
\ Z2 = reward pre-check
\ Z4 = stop recording
\ Z5 = start reward delivery
\ Z6 = reward was given
\ Z7 = reward was not given
\ Z8 = pump finished reward delivery
\ Z9 = ready for new reward

\ ****************************
\ K-PULSES to control yoked procedure
\ K1 (-3)  : start experiment
\ K4 (-6)  : stop experiment
\ K7 (-9)  : start pump
\ K10 (-12): stop pump
\ K13 (-15): turn on cue light
\ K16 (-18): turn off cue light
\ K19 (-21): turn on house light
\ K22 (-24): turn off house light
\ K25 (-27): advance session

\ K30 (-32): ABORT (something went wrong)

\ *******************************************************
\ ** STATES
\ *******************************************************

\ ****************************
\ INITIALIZATION STATE
S.S.1,
S1,						\ set default values
       0.01": SET X = 1;
              SET W(3) = ^InjRate;
              SET H(0) = ^activeNP;
              SET S(1) = ^timeOut;
              SET S(2) = ^sessionLength;
              SET S(8) = ^NPlightOn;
              SET W(1) = ^DrugConcentration / 10;
              SET W(2) = ^InfusionAmount / 10;
              SHOW 1,Remaining time (min):, 0;
              SHOW 2,Actives:, 0;
              SHOW 3,Inactives:, 0;
              SHOW 4,Rewards:, 0;
              SHOW 5,Failed rewards:, 0;
              SHOW 6,Session time (min), 0;
              SET C(I(0)) = -987.987;            \ seal arrays
              SET D(I(1)) = -987.987;
              SET K(I(2)) = -987.987;
              SET E(I(3)) = -987.987;
              SET F(I(3)) = -987.987;
              LOCKON ^HouseLight ---> S2         \ indicate to operator that box is loaded
                                                 \      -> no drug available
S2,
       #START: K(BOX);
               LOCKOFF ^HouseLight;
               SET S(0) = (W(0) * W(2)) / (W(1) * W(3)); 
                                             \ pump activation time in seconds
               SET O = (^InjVolLimit / W(3)) * 1";  \ maximum active pump time
               SET P = (W(4) / W(2));       \ index for overdose control
               SET S(4) = S(0) * 1";        \ infusion time in seconds
               SET S(5) = S(1) * 1";        \ post reward time in seconds
               SET S(6) = S(2) * 1';        \ drug delivery session in minutes
               SET S(9) = S(8) * 1";        \ time data for NP light on
               IF (S(4) < O) [@VolumeOk, @VolumeBad]
                  @VolumeOk: IF (H(0) = ^right) [@True, @False]
                             @True:  SET H(2) = ^rightCueLight;
                                     SET H(1) = ^left;
                                     SET H(3) = ^right + ^outPortOffset;
                                     ON ^Reward; K(BOX + 6) ---> S4
                             @False: SET H(1) = ^right;
                                     SET H(2) = ^leftCueLight;
                                     SET H(3) = ^left + ^outPortOffset;
                                     ON ^Reward; K(BOX + 6) ---> S4
                  @VolumeBad: K(BOX + 29); 
                              SHOW 2, Volume TOO HIGH, 0;
                              LOCKON ^HouseLight ---> STOPKILL  \ problem at the beginning: ABORT
S4,
       S(4)#T: OFF ^Reward; K(BOX + 9); Z1 ---> S2 
                               \ after flush start drug sessions with Z1 pulse

\ ****************************
\ DRUG SELF ADMINISTRATION SESSION
\ After FR X the cue light above the nose poke hole comes on and one second
\ later the drug is delivered.
S.S.3,
S1,
       #Z1: CLEAR 1,2;
            SHOW 2, Actives:, 0 ---> S2
S2,
       X#RH(0): Z2 ---> S3 \ give reward with Z2 pulse after X responses on active hole
S3,
       #Z6: IF (S(5) > S(9)) [@True, @False]
            @True:  SET S(10) = S(5) - S(4) - 0.01";
                    IF (S(10) > 0) [@TimeOut, @Pump]
                        @TimeOut: ---> S5
                        @Pump: ---> S4
            @False: SET S(10) = S(9) - S(4) - 0.01";
                    IF (S(10) > 0) [@Wait, @Pump]
                        @Wait: ---> S5
                        @Pump: ---> S4
       #Z7: ---> S7        \ no reward, check dose before allowing more rewards
S4,
       #Z8: Z9 ---> S2     \ wait for pump and be ready again
S5,
       #Z8: ---> S6        \ wait for pump ...
S6, 
       S(10)#T: Z9 ---> S2 \ ... and until the cue light is out, before being ready again

S7,
       1": IF (P <= I(2)) [@CkDose, @OkDose]	\ no need to check for volume, bad volume stops
              @CkDose: SET Q(2) = I(2) - P;      \ check every second if over-
                       SET Q(3) = (T - K(Q(2))) / 60;   \ dose condition still
                       IF (Q(3) > W(5)) [Z9] ---> S2 \ in effect
              @OkDose: Z9 ---> S2

	
\ ****************************
\ NOSE POKE RECORDING
\ Continous nose poke recording [H(0) is active hole, H(1) the inactive hole]
S.S.4,
S1,
       #START: ---> S2
S2,
       #RH(0): ADD A; 
               SHOW 2, Actives:, A;
               SET C(I(0)) = T;         \ get the time since experiment start
               ADD I(0);                \ count every nose poke press
               SET C(I(0)) = -987.987 ---> SX \ seal
       #RH(1): ADD B; 
               SHOW 3, Inactives:, B;
               SET D(I(1)) = T;         \ get the time since experiment start
               ADD I(1);                \ count every nose poke
               SET D(I(1)) = -987.987 ---> SX \ seal
       #Z4: ---> S1			        \ stop recording

\ ****************************
\ NOSE POKE LIGHT CONTROL
\ The light of the active nose poke hole turns on for 4 seconds, when a reward
\ is given.
S.S.5,
S1,
        #Z5: ON H(3) ---> S2
S2,
        S(9)#T: OFF H(3) ---> S1

\ ****************************
\ OVERDOSE AND VOLUME CONTROL
\ Two checks are implemented (s.a.):
\   - the volume can not exceed ^InjVolLimit (original 30 µl) based on the 
\     activation time of the pump
\   - the total injected amount over a time frame can not be exceeded.
S.S.25,
S1,
       #Z2: SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
            SET S(4) = S(0) * 1";
            IF (S(4) < O) [@VolumeOk, @VolumeBad]
               @VolumeOk: IF (P <= I(2)) [@CkDose, @OkDose]
                             @CkDose: SET Q(0) = I(2) - P;
                                                       \ look max dose back
                                      SET Q(1) = (T - K(Q(0))) / 60;
                                      \ calculate the time difference from max
                                      \   dose in minutes
                                     IF (Q(1) > W(5)) [@TrueDrug, @FalseNoDrug]
                                        @True: Z5 ---> SX
                                    \ the max dose is outside the time interval
                                        @False: Z7;
                                             ADD G;
                                             SET E(I(3)) = 2, F(I(3)) = T;
                                             ADD I(3);
                                             SET E(I(3)) = -987.987; \ seal
                                             SET F(I(3)) = -987.987;
                                             SHOW 5,Failed rewards:, G ---> SX
                             @OkDose: Z5 ---> SX
               @VolumeBad: Z7;
                           ADD G;
                           SET E(I(3)) = 1, F(I(3)) = T;
                           ADD I(3);
                           SET E(I(3)) = -987.987;        \ seal array
                           SET F(I(3)) = -987.987;
                           SHOW 1,VOLUME TOO HIGH, 0;
                           SHOW 5,Failed rewards:, G;
                           K(BOX + 3);
                           LOCKON ^HouseLight ---> STOPABORTFLUSH


\ ****************************
\ DRUG DELIVERY
\ Need to have a different state set for delivery to make sure that it doesn't
\ end prematurely when drug session ends
S.S.26,
S1,
      #Z5:  Z6;
            K(BOX + 6);
            ON ^Reward;
            ADD J;                     \ count reward
            SET K(I(2)) = T;           \ get time stamp of reward
            ADD I(2);
            SET K(I(2)) = -987.987;    \ seal array
            SHOW 4,Rewards:, J ---> S2

S2,
     S(4)#T: OFF ^Reward; Z8; K(BOX + 9) ---> S3
S3,
     #Z9: ---> S1

\ ****************************
\ DRUG AVAILABILITY LIGHT
\ used as an indicator for drug availability for the nose poke hole
S.S.28,
S1,
       #Z1: ON H(2); K(BOX + 12); ---> S2         \ turn active cue light on
       #Z9: ON H(2); K(BOX + 12); ---> S2
S2,
       #Z2: OFF H(2); K(BOX + 15) ---> S1

\ ****************************
\ DRUG SESSION TIMER
\ The training session for the self-administration can be S(2) minutes long
S.S.29,
S1,
       #Z1: LOCKOFF ^HouseLight;
            SET N = 0;			\ reset session timer
            SHOW 1,Remaining time (min):, S(2);
            SHOW 4,Rewards:,J;
            SHOW 6,Session time (min):, N ---> S2
S2,
       1": ADD N; 
           SHOW 1,Remaining time (min):, S(2) - (N/60);
           SHOW 6,Session time (min):,N/60;
           IF (N/60 >= S(2)) [Z4] ---> S3
S3,                     \ make sure whole session terminates after full finish
       V#T: LOCKON ^HouseLight; OFF H(2); K(BOX + 3) ---> STOPABORTFLUSH

\ ****************************
\ GENERAL TIMER
\ General timer to create a time stamp for frequency calculation in seconds, 
\ incremented every 10 ms.
S.S.32,
S1,
       #START: ---> S2
S2,
       0.01": SET T = T + 0.01 ---> SX