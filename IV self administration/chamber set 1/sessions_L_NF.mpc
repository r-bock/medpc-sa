\ sessions.mpc
\ original: SNS_SASNP.mpc
\ Self-administration sessions without delay
\ --> see SNS_SATF.mpc and SNS_SAT.mpc for self administration training program
\ Written by Roland Bock @ NIAAA / NIH (2008)
\ Date SEPTEMBER 12, 2011 version 3.5

^version = 39

\ - version 3.8 (JUL 2019): removed the timing variable for the nose-poke cue light
\ - version 3.7 (SEP 2017): added syringe volume check to program
\ - version 3.6 (AUG 2017): added version info to datafile
\
\ ** Description **
\ This program implements repeated sessions for drug self-administration,
\ interrupted by intervals with no drug availability. The house light functions
\ as an indicator for the drug availability (OFF means drug, ON means no drug).
\ To give the animal a clear signal of the session ending the house light has to
\ stay on, although the rest of the outputs are turned off with the data written
\ to the disk. This is accomplished with the LOCKON and LOCKOFF signal for the
\ house light. This also means, that the ONLY way to turn them off after the
\ the session is to either cut the power to the box or use a macro to issue a
\ LOCKOFF signal to the box.
\
\ The drug is dispensed through a syringe pump after a fixed number of lever
\ presses. A post-delivery time-out period prevents the animal from overdosing.
\ A cue light above the lever indicates to the animal when a press on the lever
\ will dispense the drug. The cue light is turned of for the duration of the
\ time-out, or if the animal is in danger of overdosing, for the entered time
\ limit.
\
\ With the START of the experiment the catheter of the animal is flushed with a
\ single drug dose, which is not counted.
\
\ Animals get the same dose of drugs bases on their body weight. The dose is
\ varied by activating the pump for different times. The calculation for the
\ pump time (t) activation is
\
\ t = (body weight * unit injected) / ([drug conc] * pump rate)
\
\ The activation time is calculated in seconds.
\
\ Two safeguards are build in. The first should prevent accidental overdose,
\ i.e. the animal has to stay below a certain dose within a previous
\ time frame (default is 5 mg/kg in the previous 10 minutes). If the current
\ dose would get over this limit, no drug is dispensed.
\ The second safeguard is against injecting a two large volume. The limit for
\ this program is 80 µl. If by operator error or other the injected volume is
\ greater than 80 µl per injection, the reward is not given.
\
\ The maximum number of drug session repetitions of the drug sessions are 10.
\ The program will terminate, if more than 10 repetitions are entered.
\
\ The maximum number of responses/rewards is 10,000, then the program reaches the
\ end of its recording arrays.
\
\
\ ** Error codes **
\ To indicate the non-delivery of a reward, because it failed a check though
\ the animal fullfilled the conditions, 2 arrays, one for the error code, the
\ other for the time stamp are recorded. The codes are
\      1:    no reward because the volume exceeded the volume limit
\      2:    no reward because the dose limit within the time limit was reached
\
\ ** System specific data **
\
\ - catheter volume: 3 �l (micro liter) -> 0.5 sec pump activation to flush
\ - desired injection (reward) volume: 18 µl (micro liter) for 33g mouse
\ - pump rate with 3cc syringe: �l/s (micro-liters per second)
\ - 1 infusion should deliver 1 mg/kg drug for a 33 g mouse [1.833 mg/ml drug]
\ --> (33 g * 1 mg/kg) / (1.8333 mg/ml * 6 �l/s) = 3 seconds pump activation
\
\ ** Default operation values (which can be changed by the operator) **
\
\ nose poke ratio: 3
\ number of drug sessions: 2
\ length of post-drug time-out: 0 s
\ length of drug session: 90 min
\ length of interval: 40 min
\
\ weight of the animal: 33 g
\ drug concentration 1.5 mg/ml
\ single injection amount: 1 mg/kg
\ pump injection rate: 6 �l/s
\ maximum volume per reward: 80 �l
\ overdose control limit: 20 mg/kg within 10 min
\
\ right = right when the mouse faces the reward magazine
\ left = left when the mouse faces the reward magazine

\ ****************************
\ CONSTANTS
\ Some of the constants only give an easy access to change the default values.
\ Many of those are used to set some values in the initialization state. Since
\ constants can only be integers, some of the values are 10-fold of their real
\ value. They will be divided by 10 during the initialization.

^InjRate = 6            \ in �l/s (micro-liter per second)
^InjVolLimit = 80        \ maximum injection volume for animal in µl (micro-liter)
^InfusionAmount = 10    \ >> 10 times the real value << in mg/kg x 10
^DrugConcentration = 15    \ >> 10 times the real value << in mg/ml x 10
^DefSyringeVolume = 3   \ default syringe volume in ml (3 ml)

^numRepeats = 2        \ number of repetitions of the drug access session

^timeOut = 0            \ post-drug time out in seconds
^FR = 3                \ nose poke ratio for reward

^activeNP = 7            \ default active nose poke hole (right side)

\ ****************************
\ INPUTS
^right = 7            \ right lever input
^left = 8            \ left lever input

\ ****************************
\ OUTPUTS
^rightCueLight = 10        \ output port
^leftCueLight = 11        \ output port
^punishLight = 12        \ output port

^HouseLight = 15
^Reward = 9            \ drug delivery through syringe pump

\ ****************************
\ DEFINED VARIABLES
\ A = Array for active nose poke counter
\ B = Array for inactive nose poke counter
\ C = Array for timing data of active nose pokes
\ D = Array for timing data of inactive nose pokes
\ E = Array for error codes
\ F = Array for error code timings

\ G = Array for summation values, only for display
\       G(0) = total active nose pokes
\       G(1) = total inactive nose pokes
\       G(2) = total rewards given
\       G(3) = failed rewards

\ H = Array for active and inactive inputs and ouputs
\    H(0) = active nose port number (default is ^right)
\    H(1) = inactive nose port number (default is ^left)
\    H(2) = active cue light
\    H(3) = inactive cue light
\       H(4) = output port number for active nose poke light
\       H(5) = output port number for inactive nose poke light
\       H(6) = output port for punishment indicator
\       H(7) = program version number

\ I = Index array for lever press counter array
\       I(0) = for response counter per period
\       I(1) = for active response timing data
\       I(2) = for inactive response timing data
\       I(3) = for reward timing data
\       I(4) = for error codes
\       I(5) = advance counter to seal session arrays

\ J = Array for reward counter
\ K = Array for reward timing

\ M = Array for time counts in seconds
\        M(0) = time for drug-on session
\        M(1) = time for drug-off session
\        M(2) = time for total experiment time
\ N = Current session time in seconds

\ O = time limit for pump not to exceed
\ P = array for index range of injections to reach max limit
\     -> needed to look back into the array to get the sliding time window

\ Q = array for time calculation of overdose check
\       Q(0) = injection number which would be the limit
\       Q(1) = time difference between Q(0) and current time
\       Q(2) =

\ R = Number of Repeats
\ S = Array for timings
\       S(0) = seconds the pump is running (reward)
\    S(1) = post reward time-out in seconds
\    S(2) = drug delivery session length in minutes
\    S(3) = no-drug inter-session length in minutes
\    S(4) = time variable for infusion (reward)
\    S(5) = time variable for post reward time-out
\    S(6) = time variable for drug delivery session
\    S(7) = time variable for inter-session
\    S(8) = extra time to sync cue light and levers
\       S(9) = active nose hole light time in seconds
\       S(10) = timing variable for S(9)
\       S(11) = time difference to wait for the pump to finish

\ T = elapsed time from experiment start in 10 msec
\ U = Drug session counter
\ V = End whole session variable

\ W = Array for animal related measures
\        W(0) = weight of the animal in g
\        W(1) = concentration of the drug (reward) solution in mg/ml
\        W(2) = single unit injection in mg/kg
\        W(3) = pump rate in �l/s (micro-liter per second)
\        W(4) = maximum dose in mg/kg per time frame
\        W(5) = time frame in min for maximum dose
\        W(6)
\        W(7)
\        W(8) = infusion volume for reward
\        W(9) = current volume in the syringe
\        W(10) = running sum of total dispensed volume

\ X = Ratio of nose pokes

DIM A = 20          \ numOfRepeats * 2 [max repeats: 10]
DIM B = 20
DIM J = 20
DIM I = 5           \ array of indices for various arrays (ran out of names)
DIM G = 3
DIM C = 10000       \ time stamps of active lever presses
DIM D = 10000       \ time stamps of inactive lever presses
DIM K = 10000       \ time stamps of reward deliveries
DIM E = 10000       \ error code array
DIM F = 10000       \ error timing array
DIM M = 3
DIM Q = 3
LIST H = 7, 8, 0, 0, 0, 0, 0, 0     \ inputs and outputs of nose pokes and cuelights
LIST S = 3, 0, 90, 40, 0, 0, 0, 0, 0, 0, 0, 0, 0   \ array for timings (default values)
LIST W = 33, 2, 2, 0, 20, 10, 0, 0, 0, 0, 0  \ array for animal related measures

Var_Alias Weight of animal (g) = W(0)
Var_Alias Drug concentration (mg/ml) = W(1)

\ Var_Alias Infusion time (sec) = S(0)

Var_Alias Number of repeats = R
Var_Alias Post reward time-out (sec) = S(1)
Var_Alias Session length (min) = S(2)
Var_Alias Session interval (min) = S(3)
Var_Alias Lever press ratio = X
Var_Alias Active lever (7 for right or 8 for left) = H(0)

Var_Alias Single infusion amount (mg/kg) = W(2)
Var_Alias Syringe volume = W(9)
Var_Alias Pumping rate (ul/s) = W(3)
Var_Alias Maximum dose per time (mg/kg) = W(4)
Var_Alias Time for maximum dose (min) = W(5)

\ PRINTVARS = A, B, C, D, E, F, H, J, K, O, R, S, T, W, X
DISKVARS = A, B, C, D, E, F, H, J, K, O, R, S, T, W, X

\ ****************************
\ Z-PULSES
\ Z1 = start drug self-administration session
\ Z2 = reward pre-check
\ Z3 = start between drug session time period or terminate
\ Z4 = stop recording
\ Z5 = start reward delivery
\ Z6 = reward was given
\ Z7 = reward was not given
\ Z8 = pump finished reward delivery
\ Z9 = ready for new reward

\ ****************************
\ K-PULSES to control yoked procedure
\ Boxes 1 - 3 can be used as control boxes, while 4 - 6 are the yoked boxes
\ K1 (-3)  : start experiment
\ K4 (-6)  : stop experiment
\ K7 (-9)  : start pump
\ K10 (-12): stop pump
\ K13 (-15): turn on cue light
\ K16 (-18): turn off cue light
\ K19 (-21): turn on house light
\ K22 (-24): turn off house light
\ K25 (-27): advance session

\ K30 (-32): ABORT (something went wrong)

\ *******************************************************
\ ** STATES
\ *******************************************************

\ ****************************
\ INITIALIZATION STATE
S.S.1,
S1,                            \ set default values
        0.01": SET R = ^numRepeats;
               SET H(7) = ^version / 10;                    \ record program version
               SET X = ^FR;
               SET H(0) = ^activeNP;
               SET H(6) = ^punishLight;
               SET W(3) = ^InjRate;
               SET S(1) = ^timeOut;                            \ default: time-out
               SET W(1) = ^DrugConcentration / 10;            \ default: drug concentration
               SET W(2) = ^InfusionAmount / 10;                \ default: single infusion
               SET S(12) = (R * S(2)) + ((R - 1) * S(3));
               SET W(9) = ^DefSyringeVolume;                \ default: syringe volume
           SHOW 1,Session (min):, 0;
           SHOW 2,SA Session #, 0;
           SHOW 3,Actives:, 0;
           SHOW 4,Inactives:, 0;
           SHOW 5,Rewards (session):, 0;
           SET I(5) = I(0) + 1;
           SET A(I(5)) = -987.987;             \ seal arrays
           SET B(I(5)) = -987.987;
           SET J(I(5)) = -987.987;
           SET C(I(1)) = -987.987;
           SET D(I(2)) = -987.987;
           SET K(I(3)) = -987.987;
           SET E(I(4)) = -987.987;
           SET F(I(4)) = -987.987;
           LOCKON ^HouseLight ---> S2         \ start with light on
                                                 \      -> no drug available
S2,
        #START: K(BOX);
                LOCKOFF ^HouseLight;
                SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
                                             \ pump activation time in seconds
                SET O = (^InjVolLimit / W(3)) * 1";  \ maximum active pump time
                SET P = (W(4) / W(2));       \ index for overdose control
                SET S(4) = S(0) * 1";        \ infusion time in seconds
                SET S(5) = S(1) * 1";         \ post-reward time-out in seconds
                SET S(6) = S(2) * 1';        \ drug delivery session in minutes
                SET S(7) = S(3) * 1';        \ inter drug time-out in minutes
                IF (R > 10) [@TrueStop, @FalseStart]
                   @TrueStop: K(BOX + 29); LOCKON ^HouseLight ---> STOPKILL
                                             \ more repeats than array slots
                   @FalseStart: IF (S(4) < O) [@VolumeOk, @VolumeBad]
                                    @VolumeOk: IF (H(0) = ^right) [@True, @False]
                                                  @True:  SET H(1) = ^left;
                                                          SET H(2) = ^rightCueLight;
                                                          K(BOX + 6);
                                                          Z1 ---> SX
                                                  @False: SET H(1) = ^right;
                                                          SET H(2) = ^leftCueLight;
                                                          K(BOX + 6);
                                                          Z1 ---> SX
                                    @VolumeBad: K(BOX + 29);
                                                SHOW 2, VOLUME TOO HIGH, 0;
                                                LOCKON ^HouseLight ---> STOPKILL

\ ****************************
\ DRUG SELF ADMINISTRATION SESSION
\ FR X triggers the reward delivery. Before the reward is delivered, both safeguards (volume and
\ dose per time) are checked. The reward delivery is signaled by a Z6 pulse. Z7 signals that the
\ reward could not be given. The Z3 pulse signals the transition to the inter-session (drug-off)
\ period, which stops all dose checking and waiting for the FR X response.
S.S.3,
S1,    \ give cue that drug is available
    #Z1: CLEAR 6,6;
         CLEAR 8,10;
             SHOW 8, Actives (period):, 0, 9, Inactives (period):, 0;
             SHOW 10, Rewards (period):, 0 ---> S2
S2,
    #Z3: ---> S1       \ transition to inter period
    X#RH(0): Z2 ---> S3
                           \ give reward with Z2 pulse after X on active
S3,
        #Z3: ---> S1
        #Z6: SET S(11) = S(5) - S(4);         \ determine diff between time-out & pump time
               IF (S(11) > 0) [@TimeOut, @Pump]
                       @TimeOut: ---> S5
                       @Pump:    ---> S4
        #Z7: ---> S7            \ no reward, check for dose
S4,
    #Z3: Z9 ---> S1            \ drug session ended, inter session start
        #Z8: Z9 ---> S2
S5,
    #Z3: Z9 ---> S1
    #Z8: ---> S6            \ when pump finishes, signal readyness for new reward
S6,
    S(11)#T: Z9 ---> S2        \ wait for pump delivering the reward, then signal readyness for reward
S7,
    #Z3: Z9 ---> S1       \ stop checking when transitioning to drug-off period
    1": IF (P <= I(3)) [@Ckdose, @OkDose]    \ no need to check for volume, since everything stops
        @CkDose:  SET Q(2) = I(3) - P;        \ with to high volume
                  SET Q(3) = (T - K(Q(2))) / 60;
              IF (Q(3) > W(5)) [Z9] ---> S2
        @OkDose:  Z9 ---> S2

\ ****************************
\ RESPONSE RECORDING
\ Continous response recording [H(0) is active, H(1) is inactive side]. It seems SUMARRAY
\ does not sum the array when start and end point are 0, so I had to include the IF statement
\ to correctly display the values.
S.S.4,
S1,
    #START: ---> S2
S2,
    #RH(0): ADD A(I(0));
        SHOW 8,Actives (period):, A(I(0));
        SUMARRAY G(0) = A, 0, I(0);
        SET C(I(1)) = T;         \ get the time since experiment start
        ADD I(1);                \ count every lever press
            SET C(I(1)) = -987.987;  \ seal
            IF (I(0) = 0) [@True, @False]
                @True:     SHOW 3, Actives:, A(I(0)) ---> SX
                @False:     SHOW 3, Actives:, G(0) ---> SX

    #RH(1): ADD B(I(0));
        SHOW 9,Inactives (period):, B(I(0));
        SUMARRAY G(1) = B, 0, I(0);
        SET D(I(2)) = T;
        ADD I(2);
        SET D(I(2)) = -987.987;
        IF (I(0) = 0) [@True, @False]
            @True:    SHOW 4,Inactives:, B(I(0)) ---> SX
            @False:    SHOW 4,Inactives:, G(1) ---> SX
    #Z4: ---> S1                    \ stop recording

\ ****************************
\ OVERDOSE AND VOLUME CONTROL
\ Two checks are implemented (s.a.):
\   - the volume can not exceed ^InjVolLimit (original 80 �l) based on the
\     activation time of the pump
\   - the total injected amount over a time frame can not be exceeded.
S.S.24,
S1,
        #Z2: SET S(0) = (W(0) * W(2)) / (W(1) * W(3));
             SET S(4) = S(0) * 1";
             IF (S(4) < O) [@VolumeOk, @VolumeBad]
               @VolumeOk: IF (P <= I(3)) [@CkDose, @OkDose]
                             @CkDose: SET Q(0) = I(3) - P;
                                      \ look max dose back
                                      SET Q(1) = (T - K(Q(0))) / 60;
                                      \ calculate the time difference from max
                                      \   dose in minutes
                                      IF (Q(1) > W(5)) [@TrueDrug, @FalseNoDrug]
                                         @True:     Z5 ---> SX
                                             \ the max dose is outside the time interval
                                         @False: Z7;
                                                 ADD G(3);
                                                 SET E(I(4)) = 2, F(I(4)) = T;
                                                 ADD I(4);
                                                 SET E(I(4)) = -987.987; \ seal
                                                 SET F(I(4)) = -987.987;
                                                 SHOW 17, Failed rewards:, G(3) ---> SX

                             @OkDose: Z5 ---> SX
               @VolumeBad: Z7;
                           ADD G(3);
                           SET E(I(4)) = 1, F(I(4)) = T;
                           ADD I(4);
                           SET E(I(4)) = -987.987;        \ seal array
                           SET F(I(4)) = -987.987;
                           SHOW 17, Failed rewards:, G(3);
                           K(BOX + 3);
                           SHOW 2, Volume TOO HIGH, 0;
                           LOCKON ^HouseLight ---> STOPABORTFLUSH

\ ****************************
\ DRUG DELIVERY
\ Need to have a different state set for delivery to make sure that it doesn't
\ end prematurely when drug session ends
S.S.25,
S1,
    #Z5: Z6;                     \ announce that reward is given
         ON ^Reward;
             K(BOX + 6);
         ADD J(I(0));            \ count the reward
         SET K(I(3)) = T;        \ get the timestamp of reward
         ADD I(3);
         SET K(I(3)) = -987.987; \ seal array
          SHOW 10,Rewards (period):, J(I(0));
         SUMARRAY G(2) = J, 0, I(0);
         IF (I(0) = 0) [@True,@False]
             @True:    SHOW 5,Rewards:, J(I(0)) ---> S2
             @False:    SHOW 5,Rewards:, G(2) ---> S2
S2,
    S(4)#T: OFF ^Reward; Z8; K(BOX + 9) ---> S3        \ infusion time for pump
S3,
    #Z9: ---> S1            \ wait until ready for next reward

\ ****************************
\ DRUG AVAILABILITY LIGHT
\ used as an indicator for drug availability.
S.S.26,
S1,
        #Z1: ON H(2); K(BOX + 12) ---> S2       \ turn the active cue light on
        #Z9: ON H(2); K(BOX + 12) ---> S2
        #Z3: OFF H(2); K(BOX + 15) ---> SX
S2,
        #Z2: OFF H(2); K(BOX + 15) ---> S1
        #Z3: OFF H(2); K(BOX + 15) ---> S1       \ Z3 terminates drug session

\ ****************************
\ SYRINGE VOLUME CHECK
\ This state keeps a running total of the dispensed volume and terminates the program
\ if it reaches or exceeds the set maximum volume
S.S.27,
S1,
      #START: ---> S2

S2,
      #Z5: SET W(8) = S(0) * W(3);        \ calculate the volume of single infusion
           SET W(10) = W(10) + W(8);    \ keep running total
           IF W(10) >= (W(9) * 1000) [Z4] ---> S5

S5,
    0.01": SHOW 1, SYRINGE EMPTY, 0 ---> S1

\ ****************************
\ DRUG SESSION TIMER
\ The training session for the self-administration can be S(2) minutes long
S.S.28,
S1,
    #Z1: SET M(0) = 0;            \ reset session timer
             K(BOX + 18);
         ADD U;                 \ count number of sessions
         LOCKOFF ^HouseLight;
         SHOW 6,Remain. sess. (min):,S(2), 2,SA Session #,U;
         SHOW 10,Rewards (session):, J(I(0)) ---> S2
S2,
    1": ADD M(0);
            SHOW 6,Remain. sess. (min):, S(2) - (M(0)/60);
            IF (M(0)/60 >= S(2)) [@TrueInter]    \ time-out or terminate
           @TrueInter: ADD I(0);
                       SET A(I(0)) = 0;   \ reset seal to zero
                       SET B(I(0)) = 0;
                       SET J(I(0)) = 0;
                   SET I(5) = I(0) + 1;       \ move seal one over
                   SET A(I(5)) = -987.987;
                   SET B(I(5)) = -987.987;
                   SET J(I(5)) = -987.987;
                   Z3;
                       K(BOX + 24) ---> S1

\ ****************************
\ INTERVAL SESSION TIMER
\ Interval session without giving the animal access to the drug. Nose pokes
\ during this time are a measure of perserverance
S.S.29,
S1,
    #Z3: SET M(1) = 0;
         K(BOX + 21);
         LOCKON ^HouseLight;
         CLEAR 6,6;                  \ clear time display for current display
         CLEAR 8,10;              \ clear lever press displays and reward
         SHOW 2,Interval #, U;
             SHOW 8, Actives (period):, 0, 9, Inactives (period):, 0;
             SHOW 10, Rewards (period):, J(I(0)) ---> S2
    #Z4: ---> SX
S2,
    #Z4: CLEAR 6,6 ---> S1
    1": ADD M(1);
            SHOW 6, Remain. int. (min), S(3) - (M(1)/60);
        IF (M(1)/60 >= S(3)) [@TrueDrug]
           @TrueDrug: ADD I(0);
                      SET A(I(0)) = 0;
                      SET B(I(0)) = 0;
                      SET J(I(0)) = 0;
                  SET I(5) = I(0) + 1;
              SET A(I(5)) = -987.987;   \ seal the arrays
              SET B(I(5)) = -987.987;
              SET J(I(5)) = -987.987;
              Z1;
                      K(BOX + 24) ---> S1         \ start drug period with Z1 pulse

\ ****************************
\ TOTAL EXPERIMENT TIMER
\ Calculates the total experiment time based on the drug-on and drug-off periods
\ and displays the total remaining time for this experiment
S.S.30,
S1,
    #START: SET S(12) = (R * S(2)) + ((R - 1) * S(3));
        SET M(2) = 0;
            SHOW 1,Remaining time (min):, S(12) ---> S2
S2,
    1": ADD M(2);
        SHOW 1,Remaining time (min):, S(12) - (M(2) / 60) ---> SX
    #Z4: SHOW 1,Session time (min):, S(12) ---> S1

\ ****************************
\ EXPERIMENT CONTROL
\ Checks if the current number of sessions is smaller than the desired
\ number of sessions.
S.S.31,
S1,
    #Z3: SET V = S(4) + 1";
         IF (U >= R) [SET V = S(4) + 1"; Z4] ---> S2  \ terminate all counting processes
    #Z4: ---> S2    \ other state requested recording stop, process here

S2,            \ make sure whole session terminates after full finish
    V#T: LOCKON ^HouseLight; OFF H(0), H(2); K(BOX + 3) ---> STOPABORTFLUSH

\ ****************************
\ GENERAL TIMER
\ General timer to create a time stamp for frequency calculation in seconds,
\ incremented every 10 ms.
S.S.32,
S1,
    #START: ---> S2
S2,
    0.01": SET T = T + 0.01 ---> SX
