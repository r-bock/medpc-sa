# Operant Self-Administration Programs for Intravenous Drug Delivery #

### What is this repository for? ###

This collection of programs are all used in a drug self-administration operant tasks where the drug delivery is through a catheter in the jugular vein. This is a technical description of the behavior programs, for more detailed information of the catheter requirements, please refer to the bibliography on the [main section][1]. 
A response on the active lever will activate a syringe pump that delivers a defined volume of drug solution via a swivel mounted tether to the catheter. 

The programs are separated by the following sections based on their implemented tasks

+ _chamber set 1_  
	This is the original chamber set with a set of fixed (non-retractable) levers, cue lights, foot-shock grid and a syringe pump for reward delivery.
	
+ _chamber set 2_  
	This mimics the programs for chamber set 1, but the connections are slightly different and they have slightly different modules which warranted a separate set. In addition the levers in these chambers are retractable.

### How do I get set up? ###

Please refer to the same section in the general overview.


## Intravenous self-administration (IVSA) programs ##

The intravenous (IV) operant chamber set is fitted with two levers that activate a syringe pump. The press on the active lever will activate the syringe pump and dispense a defined concentration of solution through a line connected to a jugular catheter into the mouse. 

### Program Descriptions ###

#### Program Comments within File ####

All programs are extensively commented to provide as much information about the default values, recorded data variables and arrays and intentions. This README will only describe the latest versions of the most commonly used programs. The repository contains more versions and older programs that are not used anymore, but kept in there for completeness reasons.

One major shift has been done a few years back, when the decision was made to remove the initial, priming flush of the catheter from the programs. The first programs started each session with an unsolicited, priming flush of the catheter to ensure that it was completely loaded with the experimental drug solution. All programs that have the *_NF* in their name, do not prime the catheter, i.e. for the first reward will have a slightly less drug amount than the following ones.

#### Common Features of All Programs ####

All recorded events are stored as timestamps in an array. The timestamp is counted in 
seconds at 0.01 second resolution from the beginning of the experiment. The index into 
the event array is kept as a general counter of the total number of that specific event. 
The reward concentration is calculated based on the fixed syringe pump rate in µl 
(microliters) per second for the given syringe volume (this needs to be tested this beforehand 
or look this up in your syringe pump manual), the drug concentration in the syringe given 
in mg (milligrams) per ml (milliliter) and the entered body weight of the mouse in grams.

__*Default values:*__

pump rate: 6 µl/s for 3 ml (cc) syringe
syringe volume: 3 ml
drug concentration in syringe: 1.5 mg/ml
reward concentration: 1 mg/kg
mouse weight: 30 g

The pump time (t) is calculated using the following formula

![t = (mouse weight * infusion) / ([drug concentration] * pump rate)](ptcalculation.png)

with w = weight of the mouse (g), inf = single infusion amout (mg/kg), 
[drug] = drug concentration in the syringe (mg/ml) and pr = pump rate (µl/s). 

__*Volume safeguard*__

_Infusion volume:_ The maximum volume of a single reward is currently set to 80 µl. If the initial 
calculation for the pump activation time will exceed that limit, the program will shut 
down and the house light turn back on. This is mainly to prevent accidental typos in 
either weight or concentration variable settings to kill the mouse. If the session closes 
and the house light turns back on right after the start signal, most likely the volume 
safeguard got triggered.

_Syringe volume:_ It can happen that the animal consumes so many rewards that the syringe 
is emptied. The syringe pump shuts off, when the bottom of the plunger is reached, but there 
is no feedback to the operant chamber. This can create an extinction situation, where the 
animal correctly responds to the lever, but the pump fails to deliver the proper reward. To 
avoid this case, a counter will sum the volumes for each delivered reward and terminate the 
session, once 3 ml is reached. This assumes, that the used syringes have a 3 ml volume and 
are completely filled. 

To allow for different syringes, the named variable **Syringe volume** can be set in each
program.

__*Overdose protection*__

The programs use a overdose protection based on intake within a certain time period. 
The default value is 20 mg/kg per 10 min. If the limit is exceeded, rewards will be 
suspended. Correct ratios will be recorded as "failed rewards", i.e. the rewards should 
have been given, but have not.

__*Limitations/Specific Aspects*__

During the evolution of these programs we removed the timeout for the drug delivery, i.e. 
the animals can freely choose how much of the drug they want to take, with the notable 
exception of the before mentioned *overdose control*. Another limiting factor is the drug 
delivery through the syringe pump. The pump will need some time to deliver the correct 
volume based on the weight of the mouse. The lever presses during the delivery time will 
be recorded, but have no effect and will not counted towards the next reward. This means 
there is a technical time out period between successive rewards which will slightly differ 
between individual mice with different body weights. 

__*Connection for Yoking Procedure*__

In November 2023 I reimplemented the yoking procedure as an experimental control. This 
required not only the creation of a new program for yoking, but also the modification of 
existing programs. The current implementation is only in the 2. set and works with the 
[training2_l_nf.mpc](#markdown-header-training_l_nfmpc), [acquisition2_l_nf.mpc](#markdown-header-acquisition_l_nfmpc) 
and [sessions2_l_nf.mpc](#markdown-header-sessions_l_nfmpc) programs.

The yoking procedure utilizes the MedPC K pulses to communicate across operant boxes. MedPC V
allows for up to 100 different K pulses. The current setup uses different K pulses in 
groups of 12, i.e. operant box 1 to 12 can be used as the input box and the K pulses stay
unique. Multiple boxes can be paired on one single box. The table below lists the used
K pulses (BOX contains the number of the linked/paired box that controls the reward and session).

| K pulse | K (BOX) |
| --------------- | ------ |
| start program | K(BOX) |
| dispense reward | K(BOX + 12) |
| toggle cue light | K(BOX + 36) |
| toggle house light | K(BOX + 48) |
| end program   | K(BOX + 60) |
| error - stop  | K(BOX + 72) |


__*Workflow*__

These programs have been designed with a specific workflow in mind: Each mouse has its own
loading macro, where the experiment specific programs and variables are set. These individual 
macros are then called from a _cohort_ or _master_ macro, which determines the loading 
sequence.

_Example Master Macro_

``` 
PLAYMACRO C:\MED-PC\Macro\TE_S1_M1.MAC
PLAYMACRO C:\MED-PC\Macro\TE_S1_M2.MAC
``` 

The 2 lines above would be the contents of a macro file, where mouse 1 would be loaded first
followed by mouse 2. All the example macros listed for the various programs end with the line
```START BOXES #boxNum``` where #boxNum is the current box. This means that each macro 
terminates after starting the box, with the effect that the _master_ macro sequentially
goes through each listed macro. After playing the last macro, itself terminates and all
boxes should be running.

### Available programs ###

The Med-Associate operant chambers are configured with two levers, combined with yellow 
LED cue lights, a houselight and a syringe pump. The active lever produces the reward 
delivery activating the syringe pump. The syringe pump is connected through a suspended 
swivel to the port of the catheter of the mouse. The inactive lever has no consequences.

|set| file name | description |
| --------- | --------- | ----------- |
|1|[training_l_nf.mpc](#markdown-header-training_l_nfmpc) | training program using 1 continuous access time for drug SA |
|1|[sessions_l_nf.mpc](#markdown-header-sessions_l_nfmpc) | program using multiple drug access sessions interrupted by no access periods |
|1|[breakpoint_l_nf.mpc](#markdown-header-breakpoint_l_nfmpc) | breakpoint program (progressive ratio) |
|1|[extinction_l_nf.mpc](#markdown-header-extinction_l_nfmpc) | extinction program, set either with all cues or without |
|1|[acquisition_l_nf.mpc](#markdown-header-acquisition_l_nfmpc) | full access session similar to the training program, but the total number of rewards are capped to a set number |
|1|[DRinSession_l_nf.mpc](#markdown-header-DRinSession_l_nfmpc) | dose response measurement with variable doses during the session |
|2|[training2_l_nf.mpc](#markdown-header-training_l_nfmpc) | see above, only with extendable levers |
|2|[sessions2_l_nf.mpc](#markdown-header-sessions_l_nfmpc) | see above, only with extendable levers |
|2|[breakpoint2_l_nf.mpc](#markdown-header-breakpoint_l_nfmpc) | breakpoint program (progressive ratio) |
|2|[extinction2_l_nf.mpc](#markdown-header-extinction_l_nfmpc) | extinction program, set either with all cues or without |
|2|[extinction2_l_osts.mpc](#markdown-header-extinction2_l_ostsmpc) | extinction program with a set "light switch" for optical stimulation |
|2|[acquisition2_l_nf.mpc](#markdown-header-acquisition_l_nfmpc) | acquisition program is similar to the training, but has a maximum number of rewards to stop |
|2|[DRinSession2_l_nf.mpc](#markdown-header-DRinSession2_l_nfmpc) | dose response with variable doses during the session |
|2|[yoke2_l_nf.mpc](#markdown-header-yoke2_l_nfmpc) | yoking program, dispensed rewards are controlled by animal in paired box |
|2|[simyoke2_l_nf.mpc](#markdown-header-simyoke2_l_nfmpc) | simulated yoking program, dispenses rewards according to a preset pattern | 


#### training_l_nf.mpc ####

_program variation_: 

+ training2_l_nf &rarr; the main difference being that the chamber set 
  2 is equipped with retractable levers which the program variant accounts for. 
+ acquisition_l_nf &rarr; a maximum dose reached will end the session. 
+ acquisition2_l_nf &rarr; the chamber set 2 program extending the levers out.
+ extinction_l_nf &rarr; sets a fixed pump time for an extinction session with cues on, has
  the option to turn all cues off. 
+ extinction2_l_nf &rarr; chamber set 2 with extended levers.


The program is has been originally created to function as a training program for the 
sessions program. This program gives a single, continuous drug access session, which 
duration is operator determined. A response on the active lever will result in a reward 
delivery through the activation of the syringe pump, a response on the inactive lever 
does not have any consequences.  

**_Available Variables:_**

These variables below can be set through the `Named Variables` dialog in the MedPC program or 
through macros with a `SET ... VALUE ... BOX` statement.

+ Weight of animal (g) - needs to be set every time, best with a macro (default: **25** g)
+ Active lever (7 for right or 8 for left) - set the active side
  - [Active lever (2 for right or 1 for left) - for the variant]
	
+ Session length (min) - the length of the total session in minutes (default: **220** minutes)
+ Lever press ratio - the desired fixed ratio responding (default: **1**)

+ Drug concentration (mg/ml) - the concentration of the solution in the syringe (default: **1.5** mg/ml)
+ Post reward time-out (sec) - time-out period after infusion (default: **0** seconds)
+ Single infusion amount (mg/kg) - the reward size (drug amount) to be delivered (default: **1** mg/kg)
+ Pumping rate (ul/s) - the speed of the syringe pump (default: **6** µl/s)
+ Maximum dose per time (mg/kg) - the drug amount not to exceed within a specific time 
  window (default: **20** mg/kg)
+ Time for maximum dose (min) - the time window for the above mentioned drug limit (default: **10** minutes)
+ Syringe volume (ml) - the size of the syringe in the pump to estimate a shutoff, when the 
  syringe is empty (default: **3** ml).

**_Recorded Data:_**

_variables_

A
: count of the total _active_ responses (nose pokes or lever presses) - a single number.

B
: count of the total _inactive_ responses - a single number.

J
: count of the total _rewards_, similar to A and B

O
: time limit for the pump not to exceed as a safety measure

T
: elapsed time counter from the start of the experiment in 10 millisecond increments

X
: the fixed ratio set for this experiment

_arrays_

C
: array of the timestamps (in seconds since the start of the experiment) for _active_ responses.

D
: array of the timestamps for _inactive_ responses.

E
: array for error codes.

F
: array for error code time stamps.

H
: array for the active and inactive input ports .
: H(0) is the input port number for the _active_ lever or nose poke (default is **right**),
: H(1) is the input port number for the _inactive_ lever or nose poke (default is **left**),
: H(2) is the output port number for the _active_ cue light
: H(3) is the output port number for the _active_ nose poke hole - if it exist
: H(4) contains the version number of the program for the .sad file

K
: array for the _reward_ delivery timestamps (see also C and D)

S
: an array to hold various time variable for calculations

W
: an array to hold various animal related variables


**_Macro Example:_**

```
NUMERICINPUTBOX "Parameters for CLOZ_C2_M21" "Enter box number" "1" #boxNum
NUMERICINPUTBOX "Parameters for CLOZ_C2_M21" "Enter training day" "1" #num
LOAD BOX #boxNum SUBJ CLOZ_C2_M21 EXPT Clozapine Drug #num GROUP clozapine C2 PROGRAM TRAINING_L_NF
FILENAME BOX #boxNum CLOZ_C2_M21.sad
NUMERICINPUTBOX "Parameters for CLOZ_C2_M21" "Weight of animal (g)" "23" #weight
SET "Session length (min)" VALUE 270 MAINBOX #boxNum BOXES #boxNum
SET "Weight of animal (g)" VALUE 1 #weight MAINBOX #boxNum BOXES #boxNum
SET "Lever press ratio" VALUE 1 MAINBOX #boxNum BOXES #boxNum
SET "Active lever (7 for right or 8 for left)" VALUE 7 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum 4.5 hr session, 1 mg/kg drug; REWARD:drug;
SHOWMESSAGE "Place mouse CLOZ_C2_M21 into the chamber"
START BOXES #boxNum
```

In the above macro example the animal indentifier is `CLOZ_C2_M21` and is loaded into box 1.
The drug dose for the syringe and the single infusion dose are not explicitly set, so the 
default values (1.5 mg/ml and 1 mg/kg respectively) will be set in the program.


#### acquisition_l_nf.mpc ####

This program is a variation on the training program. It creates a single access time
session but with a maximum number of rewards to ensure all study subjects take the same
amount. This also means that all test subjects will finish the session at a different time.
The session will end after the time given for `session length` even if the maximum number of
rewards have not been reached.

**_Available Variables:_** (in addition to the variables from the training program)

+ Max drug dose (mg/kg) - the maximum dose the animal can self administer. After the dose 
is reached, the program will end (default: **21**). 

**_Recorded Data:_**

see [training_l_nf.mpc](#markdown-header-training_l_nfmpc)



#### sessions_l_nf.mpc ####

This is has been the main program in the self-administration studies. The drug access 
times are interrupted by no access times to measure the restraint or lack thereof the mouse
develops after repeated sessions. 

**_Available Variables:_**

_identical to training program_
+ Weight of animal (g) - weight of the animal in grams (default: **33** g)
+ Drug concentration (mg/ml) - concentration of the solution in the syringe (default: **1.5** mg/ml)
+ Single infusion amount (mg/kg) - infusion amount (default: **1** mg/kg)
+ Lever press ratio - fixed lever press ratio (default: **3**)
+ Post reward time-out (sec) - time-out after given reward (default: **0**)
+ Active lever (7 for right or 8 for left) - select active lever side
  - [Active lever (2 for right or 1 for left) - for the variant]

+ Syringe volume - maximum volume of the syringe (default: **3** ml)
+ Pumping rate (ul/s) - syringe pump rate for the given syringe
+ Maximum dose per time (mg/kg) - maximum dose per time limit for overdose control
+ Time for maximum dose (min) - time limit for overdose control

_different from training program_
+ Number of repeats - number of **drug access** periods (default: **2**)
+ Session length (min) - the length of a single **drug on** period in minutes
+ Session interval (min) - the length of the drug off interval period between the drug access periods

**important note:** this program does **not** set the total session length! The total session length 
will be automatically calculate from the number of drug on and off periods and their lengths. The 
manual calculation of the total session length is `total = R * drug ON + (R - 1) * drug OFF`, 
with _R_ being the number of repeats, _drug ON_ the session length in minutes and _drug OFF_ 
the session interval in minutes.

**_Recorded Data:_**

In addition to the data already described for the training program, this will also save 
the number of repeats

R
: Number of repeats of the drug on period

**_Macro Example:_**

```
NUMERICINPUTBOX "Parameters for CLOZ_C3_M42" "Enter box number" "2" #boxNum
NUMERICINPUTBOX "Parameters for CLOZ_C3_M42" "Enter day" "6" #num
LOAD BOX #boxNum SUBJ CLOZ_C3_M42 EXPT Clozapine #num GROUP CLOZ_C3 saline PROGRAM sessions2_L_NF
FILENAME BOX #boxNum CLOZ_C3_M42.sad
NUMERICINPUTBOX "Parameters for CLOZ_C3_M42" "Weight of animal (g)" "24" #weight
SET "Number of repeats" VALUE 2 MAINBOX #boxNum BOXES #boxNum
SET "Session length (min)" VALUE 135 MAINBOX #boxNum BOXES #boxNum
SET "Session interval (min)" VALUE 90 MAINBOX #boxNum BOXES #boxNum
SET "Weight of animal (g)" VALUE #weight MAINBOX #boxNum BOXES #boxNum
SET "Active lever (2 for right or 1 for left)" VALUE 2 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum 4.5 access time (6 total session), 1 mg/kg drug; REWARD:drug;
SHOWMESSAGE "Place mouse CLOZ_C3_M42 into the chamber"
START BOXES #boxNum
```

In the above macro example the animal indentifier is `CLOZ_C3_M42` and is loaded into box 2
for experiment day 6. The fixed ration, drug dose for the syringe and the single infusion dose 
are not explicitly set, so the default values (FR 3, 1.5 mg/ml and 1 mg/kg respectively) will 
be used in the program. 

The drug on session is 135 minutes long, the drug off intervall session 90 minutes long. The 
number of repeats is set to 2, i.e. we have 2 x 135 minutes long drug available phases with 
a single 90 minutes drug off interruption. This comes out to a total session length of 360 minutes
(6 h) with 270 minutes (4.5 h) total access time. 



#### breakpoint_l_nf.mpc ####

This program implements a progressive ratio schedule in an continuous drug access session. 
The ratio increase is based on the article _Progressive ratio schedules in drug 
self-administration studies in rats: a method to evaluate reinforcing efficacy_ by 
[Richardson and Roberts](https://pubmed.ncbi.nlm.nih.gov/8794935/) (1996) and follows this equation:

![response ratio = \[5e^(_infusion number_ \* 0.2)\] - 5](bp_equation.png) 

(with _rr_ = response ratio in _inj_ = the infusion number), offset by a the training or 
sessions fixed ratio to have the mice start at the same ratio. 

The breakpoint is the last successfully reached ratio in the progressive sequence. There are
2 time limits to be set, one is the total session length after which the session will end, 
the other the breakpoint time, defaulting to 1 h, i.e. the time during which the next ratio
needs to be reached or the progamm will end.  

The progressive ratio is hardcoded into the program starting from infusion 1. Using a higher
starting FR ratio will shift the whole sequence upwards.

This program is designed to measure how much effort a mouse will put into receiving the 
drug.

**_Available Variables:_**

_identical to training program_
+ Weight of animal (g) - weight of the animal in grams (default: **33** g)
+ Drug concentration (mg/ml) - concentration of the solution in the syringe (default: **1.5** mg/ml)
+ Single infusion amount (mg/kg) - infusion amount (default: **1** mg/kg)
+ Session length (h) - 
+ Post reward time-out (sec) - time-out after given reward (default: **0**)
+ Active lever (7 for right or 8 for left) - select active lever side
  - [Active lever (2 for right or 1 for left) - for the variant]

+ Syringe volume - maximum volume of the syringe (default: **3** ml)
+ Pumping rate (ul/s) - syringe pump rate for the given syringe
+ Maximum dose per time (mg/kg) - maximum dose per time limit for overdose control
+ Time for maximum dose (min) - time limit for overdose control

_different from training program_

+ Break point time limit (h) - time limit for the breakpoint (default: **1** h)
+ Press start ratio - initial response ratio for the progressive increase (default: **3**)


**_Recorded Data:_**

In addition to the data already described for the training program (see above), the following
are recorded

_variables_

Z
: break point

_arrays_

M
: progressive sequence for the response ratio (starting at 1)

**_Macro Example:_**

```
NUMERICINPUTBOX "Parameters for C57_C3_M1" "Enter box number" "1" #boxNum
NUMERICINPUTBOX "Parameters for C57_C3_M1" "Enter day" "15" #num
NUMERICINPUTBOX "Parameters for C57_C3_M1" "Enter fixed ratio" "3" #ratio
LOAD BOX #boxNum SUBJ C57_C3_M1 EXPT C57 cocaine #num GROUP C57_L1 control PROGRAM breakpoint_L_NF
FILENAME BOX #boxNum C57_C3_M1.sad
NUMERICINPUTBOX "Parameters for C57_C3_M1" "Weight of animal (g)" "26" #weight
SET "Weight of animal (g)" VALUE #weight MAINBOX  #boxNum BOXES #boxNum
SET "Press start ratio" VALUE #ratio MAINBOX #boxNum BOXES #boxNum
SET "Active lever (7 for right or 8 for left)" VALUE 7 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum 5 h breakpoint session, 1 mg/kg cocaine; REWARD:cocaine;
SHOWMESSAGE "Place mouse C57_C3_M1 into the chamber"
START BOXES #boxNum
```

The macro example sets the breakpoint session for _mouse C57_C3_M1_ and loads it into chamber 
_1_, for experiment day _15_ with a start ratio of _3_. These parameters, including the weight
have to be entered by the operator. The active lever is the right side. With no specific `SET`
commands for the infusion dose and drug concentration in the syringe, the program will use
the default values of 1 mg/kg and 1.5 mg/ml respectively.
 

#### extinction_l_nf.mpc ####

The difference of the extinction program to the [training](#markdown-header-training_l_nfmpc) is
that both doses are set to zero and the pump time to a fixed duration. The weight has no 
influence on the pump duration and only serves to consistently record it as with the other 
programs.

**_Macro Example_**

```
NUMERICINPUTBOX "Parameters for C57_L1_M1" "Enter box number" "1" #boxNum
NUMERICINPUTBOX "Parameters for C57_L1_M1" "Enter seeking day" "25" #num
LOAD BOX #boxNum SUBJ C57_L1_M1 EXPT C57 L-Dopa #num GROUP C57_L1 control PROGRAM extinction_L_NF
FILENAME BOX #boxNum C57_L1_M1.sad
NUMERICINPUTBOX "Parameters for C57_L1_M1" "Weight of animal (g)" "26" #weight
SET "Session length (min)" VALUE 360 MAINBOX #boxNum BOXES #boxNum
SET "Weight of animal (g)" VALUE #weight MAINBOX #boxNum BOXES #boxNum
SET "Lever press ratio" VALUE 3 MAINBOX #boxNum BOXES #boxNum
SET "Extinction with cues (enter 1)" VALUE 0 MAINBOX #boxNum BOXES #boxNum
SET "Active lever (7 for right or 8 for left)" VALUE 7 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum 6 h seeking session, 0 mg/kg l-dopa; REWARD:l-dopa;
SHOWMESSAGE "Place mouse C57_L1_M1 into the chamber"
START BOXES #boxNum
```

The main difference to the [training](#markdown-header-training_l_nfmpc) program is the line 
to choose to keep all cues on or off. With all cues on (1), the lights and syringe pump will 
be activated, with all cues off (0), as in this example, all lights are off and the pump will
not be activated. The fixed ratio is explicitly set to 3 to change it from the default of 1.
The house light will still turn on once the program is loaded, turn off during the session 
and turn back on when the session is finished. 


#### extinction2_l_osts.mpc ####

This is a variation of the [extinction](#markdown-header-extinction_l_nfmpc) program shown above. 
It adds a predetermined "switch" to generate a pulse pattern for a connected TTL box to control
a laser or LED controller. The pattern is currently _simple_ (reflected in the program name ending
**o**ptical **st**imulation **s**imple), i.e. the pattern is determined by the entered pulse 
length (in seconds) and pulse frequency (in Hz). There is only a single option to set one start and
duration for the stimulation (both in minutes). 

_Error Checking_

The program has a rudimentary error check. If there is a mismatch between the entered pulse 
length and frequency, it will terminate immediately, turn the house light back on (althought it
might appear as if it was never turned off) and display the error message "pulse length and frequency 
mismatch" in the first field for the box. This can happen, if a pulse length of 0.5 s (500 milliseconds)
is entered with a frequency of 20 Hz (which determines an interval length of 50 milliseconds). The 
pulse length always has to smaller than the interval length of the frequency.

If a single, continuous TTL pulse is desired, the pulse length and frequency should be adjusted to
generate only a single pulse. 

**_Available Variables_**

_identical to training program_

+ Weight of animal (g) - weight of the animal in grams (default: **33** g)
+ Session length (min) - session length in minutes (default: **60** min)
+ ~~Post reward time-out (sec) - time-out after given reward (default: **0**)~~ - has no effect
+ Active lever (2 for right or 1 for left) - choose the active side

_different from training program_

+ Extinction with cues (enter 1) - enter 1 to turn on the cue lights (default: **0**, i.e with_out_ cues)
+ Pulse length (sec) - length of individual TTL pulse (default: **0.5** s = 500 ms)
+ Stimulation frequency (Hz) - pulse frequency in Hz (default: **1**, i.e. once per second)
+ Stimulation start time (min) - start time of the stimulation pulse in minutes from the start of the program (default: **10** min)
+ Stimulation length (min) - duration of the stimulation in minutes (default: **10** min)

**_Recorded Data_**

In addition to the variables already described for the [training](#markdown-header-training_l_nfmpc) 
program, the stimulation session time stamps (for easier access) and the timestamps for each pulse
are recorded.

_variables_

T
: total elapsed time of the session

X
: press ratio

_arrays_

A
: counters of active presses for each phase

B
: counters of inactive presses for each phase

C
: timestamp array for active presses

D
: timestamp array for inactive presses

F
: timestamp array for the phases (stimulation on and off)

H
: array of input ports and version recording

I
: array of indicies into various arrays

J
: counters for rewards for each phase

K
: timestamp array for potentially delivered rewards, if this weren't an extinction program

L
: pulse timestamps for each pulse, coded by the last digit with .001 (start) and .002 (end) 

S
: array for timing values 

V
: array for pulse train variables

W
: array for animal related values (weight, etc)

**_Macro Example_**

```
NUMERICINPUTBOX "Parameters for OPT_C1_M1" "Enter box number" "1" #boxNum
NUMERICINPUTBOX "Parameters for OPT_C1_M1" "Enter training day" "1" #num
LOAD BOX #boxNum SUBJ OPT_C1_M1 EXPT Seeking Optical Stim #num GROUP OPT_C1 cocaine PROGRAM extinction2_L_ostS
FILENAME BOX #boxNum OPT_C1_M1.sad
NUMERICINPUTBOX "Parameters for OPT_C1_M1" "Weight of animal (g)" "23" #weight
SET "Lever press ratio" VALUE 1 MAINBOX #boxNum BOXES #boxNum
SET "Session length (min)" VALUE 60 MAINBOX #boxNum BOXES #boxNum
SET "Weight of animal (g)" VALUE #weight MAINBOX #boxNum BOXES #boxNum
SET "Active lever (2 for right or 1 for left)" VALUE 2 MAINBOX #boxNum BOXES #boxNum
SET "Pulse length (sec)" VALUE 0.5 MAINBOX #boxNum BOXES #boxNum
SET "Stimulation frequency (Hz)" VALUE 1 MAINBOX #boxNum BOXES #boxNum
SET "Stimulation start time (min)" VALUE 10 MAINBOX #boxNum BOXES #boxNum
SET "Stimulation length (min)" VALUE 10 MAINBOX #boxNum BOXES #boxNum
SET "Extinction with cues (enter 1)" VALUE 1 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum 60 min session, 0 mg/kg cocaine; REWARD:cocaine;
SHOWMESSAGE "Place mouse OPT_C1_M1 into the chamber"
START BOXES #boxNum
```


#### DRinSession_l_nf.mpc ####

This program is the latest addition to this set of program and implements a dose response 
session with variable drug doses during the session. Up to 8 individual drug periods can 
be set with up to 8 different doses. In this version (2) the length of the drug period is 
determined by the set length or the set number of maximum rewards  (default 20) - 
whichever condition is satisfied first. This is to prevent the dose response to become an
unplanned extinction session especially for the lower doses.

_Constants (default values)_

The constants listed below provide an easy way to modify the programs to one's need and 
change the default values and keep the operating macros short.

| type | value |
|-----------|------------|
|Injection rate | 6 µl/s |
|Injection volume limit | 80 µl |
|Drug concentration | 15 -> 1.5 mg/ml (needs to be 10 times higher than set value) |
|Default syringe volume | 3 ml (safety feature to shut program down when syringe is deemed empty) |
|  |  |
|timeOut | 0 (seconds) &#x02192; no time out period after infusion |
|default max rewards | 20 |
|  |  |
|activeNP | 2 (input port for the active manipulanda) |
|right    | 2 &#8594; input port for the right side |
|left     | 1 &#x02192; input port for the left side |
|right cue light | 5 &#x02192; output port for the right cue light |
|left cue light | 4 &#x02192; output port for the left cue light |
|house light | 7 &#x02192; output port for the house light |
|reward  | 9 &#x02192; output port for the syringe pump |
|reward 2 | ~~output port for second syringe pump~~ (not yet implemented) |   

**_Available Variables:_**

+ Animal weight (g) - needs to be set every time, best with a macro
+ Max rewards per dose - the maximum number of rewards per dose; the same limit for all 
  doses (default: **20**)
+ Drug concentration 1 (mg/ml) - drug concentration in the syringe (default: **1.5** mg/ml)

+ Post reward time-out (sec) - a time-out period after each given infusion in seconds (default: **0**)
+ Lever press ratio - the desired fixed ratio (default: **1**)
+ Active lever (2 for right or 1 for left) - input ports of the levers or nose poke holes (default: **right**), see constants

+ Length of 1. period (min) - duration of the first drug access period in minutes
+ Length of 2. period (min)
+ &#x022EE;
+ Length of 8. period (min)

+ Dose of 1. period (mg/kg)
+ Dose of 2. period (mg/kg)
+ &#x022EE;
+ Dose of 8. period (mg/kg)

+ Pumping rate (ul/s) - the syringe pump delivery rate (default: **6** µl/s)
+ Maximum dose per time (mg/kg) - an upper limit set to prevent overdosing (default: **20** mg/kg)
+ Time for maximum dose (min) - the time limit for the above maximum dose (default: **10** min)
+ Syringe volume - the volume of the used syringe (default: **3** ml)

_the 2 following variables below are for a not yet tested feature_

+ Drug concentration 2 (mg/ml) - drug concentration in syringe 2 - (not really tried and tested)
+ Change syringe after # doses - switch counter for syringe switch - (not really tried and tested)


#### yoke2_l_nf.mpc ####

The yoke program is an experimental control where the yoked animal is exposed to the same
context of the operant chamber, but the delivered rewards are controlled by the animal in
a paired box. This yoke program does allow the selection for an _active_ lever, though it
does not do anything special for reward delivery. If the cue light is used, it will be on 
above the _active_ lever.

The aim was to create a yoke program that can be linked to most of the self-administration
programs in this set. The session is controlled by the linked/paired box, except for the 
syringe volume. If the syringe in the yoke box runs out of solution before the paired box, 
the yoke program will end. 

The yoke program does not have a defined run time, it receives its "session ends" signal
from the paired box. If this is set incorrectly, the yoked box will be active until the 
session is manually stopped by the experimentator.

_Note:_

When the yoke box receives the START signal, it is _primed_ to wait for the "session start"
signal from the linked/paired box, i.e. the house light is still on and the levers retracted.
**Only** when the linked box is started, does the yoke box start with its session (turning 
the house light off, inserting the levers into the box and, if selected, turning the cue 
light for the "active" lever on). This was done, so that the individual boxes can still be loaded 
using individual macros for each mouse. It also means that any yoked box needs to be loaded
before the paired/linked operant box is started.


**_Available Variables:_**

+ Weight of animal (g) - needs to be set every time, best with a macro
+ Drug concentration 1 (mg/ml) - drug concentration in the syringe (default: **1.5** mg/ml)
+ Pair to box - the box number that controls this session **NO** default value, must be set

+ Active lever (2 for right or 1 for left) - input ports of the levers or nose poke holes (default: **right**), see constants
+ Cue lights (0 without or 1 with) - turn the cue lights on (1) or off (0) for this session

+ Single infusion amount (mg/kg) - reward concentration (default: **1** mg/kg)
+ Pumping rate (ul/s) - the syringe pump delivery rate (default: **6** µl/s)
+ Maximum dose per time (mg/kg) - an upper limit set to prevent overdosing (default: **20** mg/kg)
+ Time for maximum dose (min) - the time limit for the above maximum dose (default: **10** min)
+ Syringe volume - the volume of the used syringe (default: **3** ml)


**_Recorded Data_**

_variables_

A
: _active_ response counter

B
: _inactive_ response counter

G
: counter for session switches (used with E and F arrays)

T
: total elapsed time from start (i.e. when the paired box gives the start signal)

Z
: linked/paired box

_arrays_

C
: timestamps of active responses

D
: timestamps of inactive responses

E
: code for session switch

F
: timestamps for session and houselight switch/toggle


**_Macro Example_**

The example below uses default values for all the other variable listed above. This keeps the 
macro on the shorter side.

```
NUMERICINPUTBOX "Parameters for TE_M1_M1" "Enter box number" "1" #boxNum
NUMERICINPUTBOX "Parameters for TE_M1_M1" "Enter yoke day" "1" #num
LOAD BOX #boxNum SUBJ TE_M1_M1 EXPT Morphine yoke control #num GROUP Cohort 1 PROGRAM Yoke2_L_NF
FILENAME BOX #boxNum TE_M1_M1.sad
NUMERICINPUTBOX "Parameters for TE_M1_M1" "Weight of animal (g)" "21" #weight
NUMERICINPUTBOX "Parameters for TE_M1_M1" "Pair to box" "6" #link
SET "Weight of animal (g)" VALUE #weight MAINBOX #boxNum BOXES #boxNum
SET "Pair to box" VALUE #link MAINBOX #boxNum BOXES #boxNum
SET "Active lever (2 for right or 1 for left)" VALUE 2 MAINBOX #boxNum BOXES #boxNum
SET "Cue lights (0 without or 1 with)" VALUE 1 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum yoke control for morphine 1 mg/kg paired with box #link ; REWARD:morphine; PAIRED: #link ;
SHOWMESSAGE "Place mouse TE_M1_M1 into the chamber"
START BOXES #boxNum
```

#### simyoke2_l_nf.mpc ####

The simulated yoke program was written in 2024 to create a yoke control using already existing 
reward timestamps from a self-administering mouse.

In this case, a macro will be used to set the individual timestamps in the K array, that in all
the other programs holds the timestamps for the delivered rewards. There is most likely a more 
elegant solution available using Pascal background procedures, but I did not have the time to 
troubleshoot Pascal and compiler errors. I have opted instead to use an external function to 
generate the macros for me (using [Igor Pro][2], where my data analysis happens).

_Note:_

The rewards will be delivered according to the set timestamps. The inter-reward interval can be
shorter than the required pump time to deliver the reward, if the weight of the yoked mouse is
larger than the mouse providing the timestamps. In this case, the reward will be skipped/not delivered
and the timestamp of that reward recorded in the E array. The MedPC console will report the 
number of "failed rewards" to indicate the error.


**_Available Variables_**

+ Weight of animal (g) - needs to be set each time
+ Drug concentration (mg/ml) - drug concentration in the syringe (default: *1.5* mg/ml)

+ Active lever (2 for right or 1 for left) - input ports of the levers or nose poke holes (default: **right**), see constants; _this has no real effect in this program, except for determining which cue light would be on, if the cue light is desired._
+ Cue lights (0 without or 1 with) - turn the cue lights on (1) or off (0) for this session

+ Single infusion amount (mg/kg) - reward concentration (default: **1** mg/kg)
+ Pumping rate (ul/s) - the syringe pump delivery rate (default: **6** µl/s)
+ Maximum dose per time (mg/kg) - an upper limit set to prevent overdosing (default: **20** mg/kg)
+ Time for maximum dose (min) - the time limit for the above maximum dose (default: **10** min)
+ Syringe volume - the volume of the used syringe (default: **3** ml)


**_Recorded Data_**

_variables_

A
: _active_ response counter

B
: _inactive_ response counter

J
: _reward_ counter - in this case the index into the set reward timings

F
: counter for the error array E

_arrays_

C
: timestamps of active responses

D
: timestamps of inactive responses

E
: array for timestamps of the rewards that were not delivered (see error reporting)

K
: array for the delivered rewards - **this is set externally!**


**_Macro Example_**

The example below utilizes the default values for the _active side_ (right), the _cue lights_ (on), 
the _infusion amount_ (1 mg/kg) and _drug concentration_ in the syringe (1.5 mg/ml) to keep the 
number of necessary lines on the shorter side.

Setting the values for the K array is absolutely required. Also sealing the K array with last
`SET` command (value -987.987) is needed to indicate the end of the sequence. This macro has
been automatically generated with a function in Igor Pro, which takes existing timestamps of 
a previously used mouse.

```
NUMERICINPUTBOX "Parameters for SCO_M6_M61" "Enter box number" "1" #boxNum
NUMERICINPUTBOX "Parameters for SCO_M6_M61" "Experiment day" "1" #num
LOAD BOX #boxNum SUBJ SCO_M6_M61 EXPT Simulated yoke #num GROUP SCO_M6 morphine PROGRAM simyoke2_L_NF
FILENAME BOX #boxNum SCO_M6_M61.sad
NUMERICINPUTBOX "Parameters for SCO_M6_M61" "Weight of animal (g)" "23.8" #weight
SET "Session length (min)" VALUE 270 MAINBOX #boxNum BOXES #boxNum
SET "Weight of animal (g)" VALUE #weight MAINBOX #boxNum BOXES #boxNum
SET K(0) VALUE 168.53 MAINBOX #boxNum BOXES #boxNum
SET K(1) VALUE 370.57 MAINBOX #boxNum BOXES #boxNum
SET K(2) VALUE 2403.31 MAINBOX #boxNum BOXES #boxNum
SET K(3) VALUE 4514.2 MAINBOX #boxNum BOXES #boxNum
SET K(4) VALUE 7063.1 MAINBOX #boxNum BOXES #boxNum
SET K(5) VALUE 9490.22 MAINBOX #boxNum BOXES #boxNum
SET K(6) VALUE 13434.1 MAINBOX #boxNum BOXES #boxNum
SET K(7) VALUE -987.987 MAINBOX #boxNum BOXES #boxNum
COMMENT BOX #boxNum 270 min session, 1 mg/kg morphine; REWARD:morphine;
SHOWMESSAGE "Place mouse SCO_M6_M61 into chamber"
START BOXES #boxNUM
```


[1]: https://bitbucket.org/r-bock/medpc-sa/src/master/IV%20self%20administration/
[2]: https://www.wavemetrics.com/
