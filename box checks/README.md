# Med-Associates Operant Chamber Helpers #

This folder contains a bunch of small programs and macros to help with the operant chamber
maintenance or easing some workflow.

### What is in it? ###

#### Clear fields ####

This program is designed to just clear the first 2 lines of visible output for a specific
box in the MedPC program. This helps the operator to quickly see which boxes are still
running when each session is determined independently per animal. 

+ clear_fields.mpc
  The program just clears the spaces 1---10 on the MedPC program display.

+ Clear fields specific boxes.MAC:
  The corresponding macro to load the program into a specific box. 

```
NUMERICINPUTBOX "Clear fields for box" "Box Number to clear" "1" %boxNumber
LOAD BOX %boxNumber PROGRAM clear_fields
```
    
#### Sipper Cycle ####

Inserts the sipper slide into the box so the operator can properly aline the sipper to 
the front of the box to prevent the guillotine door to touch it.

+ sipperCycle.mpc

+ SipperCycling.MAC:
  The macro to load the individual boxes with the program.
  
```
LOAD BOX 1 SUBJ 1 EXPT 1 GROUP 1 PROGRAM SIPPERCYCLE
LOAD BOX 2 SUBJ 2 EXPT 1 GROUP 1 PROGRAM SIPPERCYCLE
LOAD BOX 3 SUBJ 3 EXPT 1 GROUP 1 PROGRAM SIPPERCYCLE
LOAD BOX 4 SUBJ 4 EXPT 1 GROUP 1 PROGRAM SIPPERCYCLE
LOAD BOX 5 SUBJ 5 EXPT 1 GROUP 1 PROGRAM SIPPERCYCLE
LOAD BOX 6 SUBJ 6 EXPT 1 GROUP 1 PROGRAM SIPPERCYCLE
SET "Number of cycles" VALUE 5 MAINBOX 1 BOXES 1,2,3,4,5,6
SET "Cycle interval (sec)" VALUE 0.5 MAINBOX 1 BOXES 1,2,3,4,5,6
DELAY 50
START BOXES 1,2,3,4,5,6
SHOWMESSAGE "Retract all sippers"
K 1 BOXES 1,2,3,4,5,6
```

#### portTester.mpc ####

A simple program the turns all output ports on and after 15 seconds off again.


#### footshock_tester.mpc ####

A simple program that delivers a footshock through the floor grid of the operant chamber 
every 15 seconds. The left cue light turns on 1 second before the shock delivery. The right 
cue light turns on with the shock delivery. Both turn off at the end of the shock.

The shock duration starts with 0.5 seconds for 2 shocks and then increases by 0.5 seconds
every 2nd shock thereafter. The shock intensity is set on the scambler, which is outside
the control of this program.

The main reason is for this program is to deliver defined shocks in a predictable way to 
test the correct delivery.
 