\ filename: sip_breakpoint_lb.mpc
\ Ethanol self-administration program for retractable sipper operant chambers with the
\  progressive ratio increase dependent on lever pressing and licking
\
\ A variation on Roland Bock's sip_breakpoint program v. 1.1, derived from the food_wf program
\ version 1.2:  April 3, 2017: fixed an error in the offset calculation
\ version 1.1:  March 3, 2017: compiled, debugged and tested version
\ version 1:    March 1, 2017
^version = 12

\ ** Description **
\ Self administration breakpoint determination for ethanol created for use with the retractable
\ sipper chambers. The session is a single, full access session and will be terminated by default
\ either 1 hour after the last successful ratio or after 5 hours total time.
\ The experimenter determines which side is the active and inactive (per default the right lever,
\ from the animal's view, is the active lever).
\
\ Licks from the lickometer will be recorded during the sipper extended and retracted
\ state. Although the sipper should be mounted that the sipper is inaccessible during the
\ retracted state, it might be possible for a mouse to reach it, if it is not mounted
\ correctly. To ensure proper user feedback the recording continues during both states.
\ Only if licks in the retracted state have been recorded, will they be shown in the
\ display.
\
\ The session starts when the house light turns off and the active cue light illuminates.
\
\ The progressive ratio follows the published work from Robinson (see breakpoint program for
\ IV self-administration) and will be offset by the inital set FR ratio, e.g. if the first
\ ratio should be 3, the rest of the curve is shifted upwards by 2 presses.
\ In this variation the ratio increase is dependent on the engangement with the sipper.
\
\ The experimenter has a choice when the breakpoint timer will reset, after the successful
\ ratio and sipper extension, or after the required minimum licks have been recorded on the
\ extendend sipper. This can set by a simple binary flag in the alias dialog using the
\ "Reset BP Timer after licks" entry and setting it to either 0 (reset after presses) or
\ 1 (reset only after minimum licks have been recorded on extended sipper) [default].

\ ** Default operation values (which can be changed by the operator)**
\
\ start lever press ratio: 1
\ length of breakpoint session: 300 min -> session length
\ length of breakpoint time: 60 min -> breakpoint time
\
\ sipper access time: 10 seconds
\ recorded reward concentration: 20%
\
\ right = right when the mouse faces the sipper
\ left = left when the mouse faces the sipper

\ *********************************
\ CONSTANTS
\ This section is to make default value changes simpler.

^activeLP = 2          \ default setting; right lever is active
^fixedRatio = 1        \ default setting; fixed ratio - FR

^accessTime = 10       \ default setting; sipper access time in seconds
^sessionLength = 300   \ default setting; session length time in minutes
^defBPTime = 60        \ default break point time in min, 1 h to reach BP
^defStartR = 1         \ default starting ratio
^doorDelay = 15        \ default delay time (rise time of the door) times 10 in seconds
^minLicks = 3		   \ default minimum number of licks to advance to the next ratio

\ *********************************
\ INPUT PORTS
^right = 2           \ right lever input
^left  = 1           \ left lever input

^lick  = 3           \ lickometer response input
\ *********************************
\ OUTPUT PORTS
^rightCueLight = 5   \ right cue light port
^leftCueLight  = 4   \ left cue light port

^HouseLight    = 7   \ output port for the house light
^sipper  = 3         \ sipper control
^door = 6			 \ guillotine door

\ *********************************
\ DEFINED VARIABLES
\ A = Counter for active lever presses
\ B = Counter for inactive lever presses
\ C = Array for timing data of active lever presses
\ D = Array for timing data of inactive lever presses

\ E = array for response ratios
\ F

\ G = Array to record sipper access time

\ H = Array for active and inactive inputs and outputs
\      H(0) = active lever press (default is ^right)
\      H(1) = inactive lever press (default is ^left)
\      H(2) = drug availability light

\ I = Ratio recording
\ J = Sipper access counter
\ K = Array for sipper timing

\ L = Counter for lickometer responses while sipper is extended
\ M = Array for timing data of extended sipper lickometer responses

\ N = Current session time in seconds

\ O = Counter of responses left for the current ratio

\ P = Total amount of time sipper is extended

\ Q = signal if sipper is extended,
\     where Q = 1 for sipper retracted (end session)
\           Q = 0 for sipper extended (wait to end)

\ R = Counter for lickometer responses while sipper is retracted (ideally should not happen)

\ S = Array for timings
\     S(0) = session length
\     S(1) = timing variable for sipper extension
\     S(2) = timing variable for session length
\     S(3) = timing variable for sipper access time

\ T = elapsed time from experiment start in 10 ms intervals

\ U = Array for timing data of retracted sipper lickometer responses

\ V = breakpoint timer

\ W = Array for animal-related measures
\     W(0) = reward concentration in %
\     W(1) = animal weight in g

\ X = Ratio of nose pokes (FR)(default is ^fixedRatio)
\
\ Y = Offset variable for the poke ratio to start
\ Z = last achieved poke ratio for easy access and display

\ Arrays
DIM C = 10000              \ time stamps for active lever presses(A)
DIM D = 10000              \ time stamps for inactive lever presses(B)
DIM K = 10000              \ time stamps for sipper access (J)
DIM G = 10000              \ record of access times
DIM M = 100000             \ time stamps for lickometer responses (extended L)
DIM U = 100000             \ times stamps for lickometer responses (retracted R)
List H = 2, 1, 5, 4, 0     \ inputs of levers, output of cuelights
LIST E = 1, 2, 4, 6, 9, 12, 15, 20, 25, 32, 40, 50, 62, 77, 95, 118, 145, 178, 219, 268, 328, 402, 492, 603, 737, 901, 1102, 1347, 1646, 2012, 2459, 3004, 3670, 4484, 5478, 6692, 8175, 9986, 12198, 14900, 18200
List S = 120, 10, 0, 0, 0, 0, 0  \ array for timings (default)
List W = 20, 20, 0, 3, 1      \ array for animal-related measures

Var_Alias Session length (min) = S(0)
Var_Alias Sipper access time (sec) = S(1)
Var_Alias Breakpoint time (min) = S(4)
Var_Alias Start ratio = X
Var_Alias Minimum number of licks = W(3)
Var_Alias Active lever (2 for right or 1 for left) = H(0)

Var_Alias Animal weight (g) = W(1)
Var_Alias Reward concentration (%) = W(0)
Var_Alias Door move time (s) = S(5)
Var_Alias Reset BP Timer after licks = W(4)				\ flag to choose when the BP timer should
														\   be reset

DISKVARS = A, B, C, D, E, G, H, I, J, K, L, M, P, R, S, T, U, W, X, Y, Z
\ ************************************
\ Z-PULSES
\ Z1 = start self-administration session
\ Z2 = sipper extended
\ Z3 = sipper retracted
\ Z4 = stop recording
\ Z5 = open door, before sipper extends
\ Z32 = signal to end session and save to disc

\ ********************************************************************************
\ **STATES
\ ********************************************************************************

\ *************************************
\ INITIALIZATION STATE
S.S.1,                                       \ set default values
S1,
  0.01": SET H(4) = ^version / 10;			  \ record version number as decimal
         SET X = ^fixedRatio;                \ set start ratio to default start ratio
         SET H(0) = ^activeLP;
         SET S(0) = ^sessionLength;
         SET S(1) = ^accessTime;
         SET S(4) = ^defBPTime;
         SET S(5) = ^doorDelay / 10;		 \ default to constant x 10 to enable 1.5
         SET W(3) = ^minLicks;               \ set the default number of licks to increase ratio
         SET Q = 1;                          \ signal to quit the session or not
         OFF ^sipper;                        \ retract sipper
         OFF ^door;
         SET C(A) = -987.987;                \ seal arrays
         SET D(B) = -987.987;
         SET M(L) = -987.987;
         SET K(J) = -987.987;
         SET G(J) = -987.987;
         SET U(R) = -987.987;
         LOCKON ^HouseLight ---> S2          \ indicate to operator that box is loaded
                                             \    -> no drug available
S2,
	#START: IF (S(5) < ^doorDelay / 10 ) [@Right, @Wrong]
				@Right:	SET S(5) = ^doorDelay / 10 ---> S3
				@Wrong: ---> S3
S3,
    0.01":  LOCKOFF ^HouseLight;
    	    SET I = 0;			     \ initialize index for ratio sequence
            SET S(2) = S(0) * 1';    \ session time in minutes
            SET S(6) = S(5) * 1";	 \ convert door delay time into MedPC seconds
            SET Y = X - 1;		     	\ ratio offset for the following ratios
            SET X = E(I) + Y;		     \ set the start ratio of the sequence
            IF (H(0) = ^right) [@Right, @Left]
               @Right: SET H(1) = ^left;
                       SET H(2) = ^rightCueLight;
                                   Z1; ---> S4
               @Left:  SET H(1) = ^right;
                       SET H(2) = ^leftCueLight;
                                   Z1; ---> S4
S4,
	#Z32: ---> SX

\ ************************************
\ DRUG SELF-ADMINISTRATION SESSION
\ After FRX the cue light above the lever comes on and one second later the sipper is extended
S.S.3,
S1,
    #Z1:     SHOW 5, Ratio:, X ---> S2
S2,
    X#RH(0): IF (W(4) = 0) [@RESET, @NOT]		\ reset BP timer with successful lever press
    			@RESET: Z5; SET V = 0 ---> S3
    			@NOT:   Z5 ---> S3	    \ got ratio, send signal to extend sipper and start
    			                        \   checking for licks and switch to waiting state
    			                        \   until sipper retracts again

    #Z4:     ---> S1                    \ stop recording
S3,
    #Z3:     SHOW 5, Ratio:, X ---> S2  \ wait for sipper retraction before counting for FR again
    #Z4:     ---> S1                    \ stop recording

\ ***********************************
\ LICK CONTROL
\ Only increase ratio after specific number of licks to ensure engagement
S.S.4,
S1,
	#Z5:    ---> S2
S2,
	W(3)#R^lick: ADD I;			\ increase index to the next ratio
	             SET Z = X;	    \ keep prev ratio -> will be the breakpoint if current not reached
	             SET X = E(I);  \ get the next Ratio
	             SET X = X + Y; \ offset the ratio accordingly to initial start ratio
                     IF (W(4) = 1) [@RESET, @NOT]
                          @RESET: SET V = 0 ---> S1
                          @NOT:   ---> S1

\ ***********************************
\ SIPPER ACCESS
\ Sipper delivery into chamber and recording
S.S.5,
S1,
   #Z2: SET S(3) = S(1) * 1";          \ set sipper access time
        SET K(J) = T;                  \ get reward timing
        SET G(J) = S(1);               \ record sipper access time in array
        SUMARRAY P = G, 0, J;          \ calculate total sipper time
        ADD J;						   \ increase index to seal arrays
        SET K(J) = -987.987;           \ seal array
        SET G(J) = -987.987;           \ seal array
        SHOW 10, Total access time:, P;
        SET Q = 0;                     \ signal sipper extended
        ON ^sipper ---> S2             \ extend sipper
S2,
   S(3)#T: OFF ^sipper; SET Q = 1; Z3 ---> S1


\ **********************************
\ LEVER PRESSING RECORDING
\ Continuous lever press recording [H(0) is active lever, H(1) is inactive lever]
S.S.6,
S1,
    #START: ---> S2
S2,
    #RH(0): SET C(A) = T;                    \ get the time since experiment start
            ADD A;
            SET C(A) = -987.987;             \ seal
            SHOW 3, Actives:, A ---> SX

    #RH(1): SET D(B) = T;                    \ get the time since experiment start
            ADD B;
            SET D(B) = -987.987;             \ seal
            SHOW 8, Inactives:, B ---> SX

    #Z4: ---> S1                             \ stop recording


\ **********************************
\ LICKOMETER RECORDING
\ Continuous lickometer recording based on the state of the lickometer. Licks during the
\ extended and retracted state of the sipper are recorded into different variables/arrays.
\ This should allow for better feedback during the program operation.
S.S.7,
S1,
    #START: ---> S2
S2,
    #Z2: ---> S6	                        \ sipper gets extended, so switch recording state

    #R^lick: SET U(R) = T;					\ get time stamp of response
             ADD R;							\ increment index
             SET U(R) = -987.987;			\ seal array
             SHOW 9, retract. Licks:, R ---> SX

S6,
    #Z3: ---> S2                	    	\ sipper gets retracted, so switch recording state

    #R^lick: SET M(L) = T;                  \ get the time since experiment start
             ADD L;                         \ increment index
             SET M(L) = -987.987;           \ seal
             SHOW 4, ext. Licks:, L ---> SX

\ *************************************
\ LEVER PRESS RATIO DISPLAY COUNTER
\ This state's job is to provide visual feedback for the user how many responses
\ have already been counted toward the next ratio.
S.S.8,
S1,
    #Z1:    SET O = X;
            SHOW 2, Presses left:, O ---> S2 \ wait for the state set 1 to calculate all values

S2,
    #RH(0): SUB O;
            SHOW 2, Presses left:, O ---> SX
    #Z2:    SHOW 2, Presses left:, 0 ---> S4
    #Z4:    ---> S1                             \ stop recording

S4,
    #Z3:    SET O = X;                     \ reward given, reset counter.
            SHOW 2, Presses left:, O ---> S2
    #Z4:    ---> S1                        \ stop recording

\ ************************************
\ GUILLOTINE DOOR CONTROL
\ The door is necessary to cover the sipper hole to avoid access to the sipper (lickometer
\ recordings)
S.S.10,
S1,
	#START: ---> S2
S2,
	#Z5: ON ^door ---> S3		\ open door after correct response
	#Z4: ---> S1
S3,
	S(6)#T: Z2 ---> S4			\ wait delay time, then extend sipper
	#Z4: ---> S1
S4,
	#Z3: ---> S5				\ wait for sipper retraction signal
	#Z4: ---> S1
S5,
	0.01": OFF ^door ---> S2	\ close door after retraction signal (door is much slower
								\    (than sipper, so the sipper is already retracted,
								\    before the door could interfere with it)

\ ************************************
\ SIPPER AVAILABILITY LIGHT
\ Used as an indicator for access to sipper time, sits above the active lever press

S.S.25,
S1,
      #Z1: ON H(2)  ---> S2                  \ turn active cue light on
S2,
      #Z2: OFF H(2) ---> S3                  \ sipper extended, so turn off light
S3,
      #Z3: ON H(2)  ---> S2                  \ sipper retracted, turn light back on until next reward
      #Z4: OFF H(2) ---> S1                  \ stop responding when session ends

\ ***********************************
\ BREAKPOINT TIMER
\ Stops the session after specified time with no pokes towards next breakpoint

S.S.26,
S1,
        #START: SET V = 0 ---> S3
        #Z2:    SET V = 0 ---> S3
S3,
        1":     ADD V;
                IF (V/60 >= S(4))  [Z4] ---> S4

S4,
        0.01": LOCKON ^HouseLight; OFF H(2); Z32 ---> SX

\ ***********************************
\ TIME DISPLAY HELPER
\ This state decides how long the current session will last and display it to the user

S.S.27,
S1,
       #START: CLEAR 1, 1 ---> S2
S2,
       1": IF ( (S(0) - (N/60)) < (S(4) - (V/60) )) [@Total, @Breakpoint]
                  @Total: SHOW 1,Remaining time (min):, S(0) - (N/60) ---> SX
                  @BP:    SHOW 1,Remaining time (min):, S(4) - (V/60) ---> SX
       #Z4: SHOW 1,Remaining time (min):, 0 ---> S1


\ ************************************
\ TOTAL SESSION TIMER
\ The session can be S(0) minutes long
S.S.29,
S1,
      #Z1: LOCKOFF ^HouseLight;
           SET N = 0;                        \ reset session timer
           SHOW 1, Remaining time (min):, S(0);
           SHOW 6, Session time (min):, N ---> S2
S2,
      1": ADD N;
          SHOW 6, Session time (min):, N/60;
          IF (N/60 >= S(0))  [Z4] ---> S3
S3,
      1": IF (Q = 1) [Z32] ---> SX             \ check if sipper is out before stopping session

\ ************************************
\ SESSION VARIABLE DISPLAY
\ Changes the display of variables for easy access and ends the session
S.S.31,
S1,
	#START: SHOW 1, Remaining time (min):, 0;
                SHOW 2, Presses left:, 0;
                SHOW 3, Actives:, 0;
                SHOW 4, ext. Licks:, 0;
                SHOW 5, Ratio:, 0;
                SHOW 6, Session time (min):, 0;
                SHOW 8, Inactives:, 0;
                SHOW 9, retract. Licks:, 0;
                SHOW 10, Total access time:, 0 ---> S2

S2,                                      \ make sure whole session terminates after full finish
    #Z32: 	SHOW 1, Actives:, A;
                SHOW 2, Inactives:, B;
                SHOW 3, ext. Licks:, L;
                SHOW 4, retract. Licks:, R;
                SHOW 5, Breakpoint:, Z;
                CLEAR 8,9;
                LOCKON ^HouseLight; ON ^door; OFF H(2) ---> STOPABORTFLUSH

\ ************************************
\ GENERAL TIMER
\ General timer to create a time stamp in seconds, incremented every 10 ms
S.S.32,
S1,
  #START: ---> S2
S2,
  0.01": SET T = T + 0.01 ---> SX
