\ file name: sipperCycler.mpc
\ Safe sipper cycling program when guillotine doors are installed
\ Version 2
\
\ * version 2 (June 2015): changed program to start into the cycling and stop with the 
\         open door and extended slide so that the sipper can be installed. Needs a 
\		  operator input or default wait time of 10 min to finish.
\ Written by Roland Bock (May 2015)
\
\ * Description *
\ This program is designed to cycle the sippers automatically before the first round of 
\ experiments without damaging the guillotine doors. The cycling is a necessary test to
\ ensure the proper insertion of the sipper into the behavior chamber.
\
\
\ *** CONSTANT DEFINITIONS ***
\
\ * Default Values *
^numCycles = 5				\ default 5 cycles
^cycleInterval = 1			\ default 1 second
^waitForInput = 10			\ default wait 10 min before closing

^doorDelay = 2				\ default 1 second to make sure the door is out of the way

\ * Output Ports *
^sipper = 3					\ sipper port
^door = 6					\ guillotine door port
^house = 7					\ house light port
^leftCueLight = 4
^rightCueLight= 5

DIM T = 4

\ *** ALIAS DEFINITIONS ***
Var_Alias Number of cycles = X
Var_Alias Cycle interval (sec) = I
Var_Alias Start time offset (sec) = O
Var_Alias Wait time for input (min) = W

\ *** K PULSES ***
\ K1     Wait for sipper loading to finish

\ *** Z PULSES ***
\ Z1     Start of cycling
\ Z2     Done cycling
\ Z3     Turn on all box lights for check
\ Z4     Program is shutting down

\ ****** START & INITIALIZATION STATE ******
S.S.1,
S1,
	0.01":  SET I = ^cycleInterval;
	        SET X = ^numCycles;
	        SET W = ^waitForInput;
	        OFF ^sipper;
	        OFF ^door ---> S2
	       
S2,
	#START: SET T(0) = O * 1";		\ timing variable for delayed start
	        SET T(1) = I * 1";		\ timing variable for cycle interval
	        SET T(2) = ^doorDelay * 1";	\ timing variable for door rise time
			SET T(3) = W * 1';		\ timing variable for operator input
			Z1 ---> SX

\ ****** SIPPER CYCLING *******
S.S.3,
S1,
	#Z1:    ---> S2

S2,
	T(0)#T: ON ^house;
	        ON ^door ---> S3

S3,
	^doorDelay": ON ^sipper ---> S4
	
S4,
	0.1":   OFF ^sipper;
	        ADD N;
	        IF (N >= X) [@Stop, @Continue] 
	        			@Stop:     ---> S10
	        			@Continue: ---> S5
	
S5,
	T(1)#T: ON ^sipper ---> S4
	
S10,
	0.01":  ON ^door ---> S11

S11,
	^doorDelay": ON ^sipper;
		         Z2 ---> S1
	        
\ ****** WAIT FOR OPERATOR INPUT ******
\ The cycle ends with the door open and the sipper slide extended, so the operator
\ can attach the sipper correctly to the slide
S.S.10,
S1,
	#START: ---> S2

S2,
	#Z2: ---> S3

S3,
	#K1: Z3 ---> S1
	T(3)#T: Z3 ---> S1

\ ****** OUTPUT PORT CHECK ******
\ While we are in the process of checking, also just briefly turn on the other output
\ ports to make sure they are all working
S.S.30,
S1,
	#Z3:    ---> S2

S2,
	0.01":  ON ^rightCueLight;
	        ON ^leftCueLight ---> S3

S3,
	2":     OFF ^rightCueLight;
	        OFF ^leftCueLight;
	        Z4 ---> S1
	        
\ ****** TERMINATE ******
\ Terminate the session without recording anything
S.S.31,
S1,
	#Z4:    OFF ^sipper;
		    OFF ^door;
		    OFF ^house ---> STOPKILL
	        
	