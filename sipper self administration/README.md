# Operant Self-Administration Programs for Oral Drug Delivery #

### What is this repository for? ###

This collection of programs are all used in drug self-administration operant tasks. The programs listed under IV self-administration activate syringe pumps to deliver a defined drug dose with a lever press on a defined ratio. The programs listed under sipper self-administration control operant boxes with a retractable sipper.

The initial programs (training, sessions and breakpoint) derive from earlier versions of the [IV self administration][iv] programs, so share common features. 

### How do I get set up? ###

Download the individual files and place them into the MPC folder inside your _Med-PC V_ setup on your computer that is connected to the chambers. Modify the port settings according to your own boxes setup (or change your boxes to match the ports of the program). The port settings are listed after the description of the program. It is best to use the _Trans V_ program that comes with _Med-PC_ to change and compile them. The programs need to be compiled before they can be used. The _Med-PC V_ program itself need to be closed for the compilation to work. Once they are compiled, the programs will show up in the "Open session" dialog in _Med-PC V_.

These programs have been written for _Med-PC IV_ and successfully compiled with _Med-PC V_. 


## Program Descriptions ##

### Hardware setup ###

The programs have been written with the standard modular mouse chamber [ENV-307A-CT][mc] in mind using the [SmartCtrl Package (4 in/8 out) DIG-716P1][mcifp] providing 4 inputs and 8 outputs. The fluid delivery is provided by a the retractable sipper w/ lixit [ENV-352-2M][lixit]. We replaced the bottle reservoir that comes with it, with a 5 ml serological glass pipette, which allows us to precisely record the consumed volume after the end of the session. The sipper is connected to a lickometer to measure contacts of the mouse with the sipper. Two retractable levers flank each side of the sipper with LED cue lights mounted above each of them. A house light is mounted on the opposite wall.

_Note_
Our sipper setup uses a custom build guillotine door to prevent any contact of the mouse during the time when the sipper is supposed to be inaccessible. We discovered that the travel way for the sipper is not large enough to prevent the mouse from reaching the sipper valve and creating contact counts for the lickometer even when the sipper is retracted outside of the chamber. The only solution to this problem has been a custom designed guillotine door for the sipper from [Med Associates][1] to close the access hole of the sipper. The potential problem now is that the sipper could jam into the door, if the timing is off. Each of the programs has a "door delay" variable that defaults to 1.5 seconds, that is needed to ensure that the door is open, before the sipper gets made accessible to the mouse.

_choice program_
The choice program (sip_choice.mpc) requires a different chamber setup as the other programs. In addition to the sipper, also a dipper [ENV-302M-S][dip] is used to deliver a competing reward option. The levers giving access to either option are mounted next to their delivery systems (sipper or dipper) on opposite walls of the chamber. 

### General definitions ##

The levers are mounted on each side of the sipper, so when referring to the right or left lever, right or left is considered from the mouse perspective, when the mouse faces the sipper. Most recorded events are stored as timestamps in an array. The timestamp is counted in seconds at 0.01 second resolution from the beginning of the experiment. 

### Self-administration programs ###

| file name | description |
| --------- | ----------- |
|sip_session.mpc | main program for self-administration |
|sip_extinction.mpc | extinction program for the session |
|sip_breakpoint.mpc | breakpoint program (progressive ratio) |
|sip_breakpoint_lb.mpc | breakpoint program checking licks before advancing the ratio |
|sip_sipTraining.mpc | sipper training program |
|sip_rr_session.mpc | random ratio program (still in testing phase)|
|(sip_footshock.mpc) | (still in testing phase) |
|         |           |
|         |           |
|sip_choice.mpc | sipper choice program (requires a different chamber setup than the other programs) |
|         |           |
|_utility_     |           |
|sipperCycle.mpc | utility program to cycle the sipper before use to ensure that they are all traveling the full length of their designed distance |
   
#### sip_session.mpc ####

This program is the main program for the suite of the sipper operant chamber programs. It provides a continues access session. One of the levers can be assigned as the active one, the other will automatically be set as the inactive one. Independent of the lever assignment, the active one will always record the data into the "active" array and the same for the inactive. 

#### sip_extinction.mpc ####

#### sip_breakpoint.mpc / sip_breakpoint_lb.mpc ####

#### sip_sipTraining.mpc ####

The program is designed to accustom the mice to the sipper as a drinking device. Both levers are inactive and extended. The sipper is permanently inserted into the chamber for the whole session and no cue lights are active. Responses on both levers and licks are recorded, but the responses on the levers have no effect. 

__*Recorded Data:*__

+ number of responses on both levers and licks
+ timestamps for the responses on the levers and the licks

#### sip_rr_session.mpc ####

This is a random ratio full access session for the sipper boxes. A lever press on the active lever triggers a randomizer that decides, based on a given ratio, if the mouse gains access or not. 

__*Available Variables:*__

+ total session length in minutes (_default_: **120 minutes**)
+ sipper access time in seconds (_default_: **10 seconds**)
+ the desired random ratio (_default_: **2**, i.e. every second press should gain access time)
+ side of the active lever (_default_: **right (2)**; the number is the port number of the lever)

+ the mouse weight (_not strictly needed for the program to work, but nice to have recorded here_)
+ reward concentration in percent (_default_: **20**, i.e. 20% ethanol)
+ door move time (_default_: **1.5 seconds**, i.e. time for the guillotine door to move out of the
the way for the sipper to move into the chamber)

__*Recorded Data:*__

+ number of active and inactive lever presses and licks
+ timestamps (in 0.1 seconds resolution from the start of the experiment) for the active
and inactive presses and the licks
+ start of each sipper accesstime (as timestamp) and their duration
+ weight of the mouse and concentration of the reward solution


#### (sip_footshock.mpc) ####

This program delivers a footshock to the mouse on the last active lever press of a successful fixed ratio. This can either be a sequence, i.e. every 2nd or 3rd access time will be shocked, or a random ratio, where the shock will be unpredictable.

In the med-associates operant chambers, both the lickometer and the footshock device use the floor grid, either as ground or to deliver the current. To avoid damaging the lickometer without any extra device, we disconnected the lickometer for this program. Because we do not record licks, I repurposed the "retracted" licks time array and counter to record the delivered footshocks.

The program also requires an extra shocker device, like the [ENV-414S][s1] or 
[ENV-414][s2]. The program can not record or control the stimulus strength, this has to be manually set by the experimentator.


__*Available Variables:*__

+ total session length in minutes (_default_: **120 minutes**)
+ sipper access time in seconds (_default_: **1.5 seconds**)
+ fixed ratio (_default_: **1**)
+ side of the active lever (_default_: **right (2)**; the number is the port number of the lever)
+ shock time in seconds (_default_: **2 seconds**)
+ choose between a shock sequence or random ratio shock (_default_: **shock sequence**)
+ the shock sequence or random shock ratio (_default_: **2**)

+ the mouse weight
+ the reward concentration in percent (_default_: **20**, i.e. 20% ethanol)
+ the door move time in seconds (_default_: **1.5 seconds**, i.e. time for the guillotine door to move
out of the way for the sipper to move into the chamber)


#### sip_choice.mpc ####

The choice program is modeled after the paper (A choice-based screening method for compulsive drug users in rats)[p1] by M. Lenoir, E. Augier, C. Vouillac et al., Current Protocols in Neuroscience, 9.44.2. In this implementation, the 2 distinct levers on opposite sides of the operant chamber control two distinct reinforcer delivery mechanisms. One is a sipper with the reinforcing fluid attached to it, the other a dipper, where a small cup with the reinforcing solution is presented to the mouse. 

The program runs through a number of sampling trials, where each choice is presented to the mouse individually, before moving to the choice trials. The number of sampling trials default to 4 but can be adjusted by the operator to experiment specification. The number of sampling trials will always 
be even, even if an odd number is entered. The next higher even number will be the number of presented sampling trials. 

The choice trials will present both levers as options, but once a choice has been made, both levers retract and the associated reward will be presented. Both delivery systems will be time based, i.e. the correct lever press will gain the mouse access to either the sipper or dipper. After a set time, which can be chosen individually for each reward and defaults to 60 seconds for both, the delivery system will retract. After the reward access time or the trial time is over, an inter-trial interval in entered, in which both cue lights are off and levers are retracted. The trial length defaults to 2.5 minutes, the inter-trial interval to 5 minutes. Responding on either lever has to be consecutive, if the fixed ratio is larger than 1. Responding on the other lever will reset the counter for the fixed ratio.

The program will end after the last choice trial is over. 

The MedPC IV program will display in the first line the remaining time for each phase, either the trial or the inter-trial interval in minutes, the remaining number of sampling or choice trials, the number of sipper choices, the number of dipper choices and the "sipper over sipper + dipper" ratio. The second line will show the total number of lever presses for the sipper, the total number of sipper choices, the total number of dipper lever presses, the total number of dipper choices, the number of no-choice trials during the choice trials and in the third line the total number of licks.

__*Limitations:*__

+ the _maximum_ number of trials (sampling and choice) can not exceed **100**

__*Available Variables:*__

+ Trial length (min): sets the individual trial lengths in minutes (_default_: **2.5** minutes)
+ Intertrial length (min): sets the inter-trial interval in minutes (_default_: **5** minutes)
+ Fixed ratio (sipper): the desired FR for the sipper (_default_: **1**)
+ Fixed ratio (dipper): the desired FR for the dipper (_default_: **1**)
+ Number of sampling trials: (_default_: **4**) independent of the entered number, the program will always present an even number 
of sampling trials. If the entered number is odd, the mouse will be presented with the next higher even number
of trials.
+ Number of choice trials: (_default_: **8**), number of choice trials. Contrary to the sampling trial any entered number will be
honored.

+ Sipper access time (sec): amount of time for the sipper to be accessible to the mouse (_default_: **60** seconds)
+ Dipper access time (sec): amount of time for the dipper to be accessible to the mouse (_default_: **60** seconds)

+ Animal weight (g): mouse body weight for easy recording with a macro, not necessary for the operation of the program
+ Reward concentration (%): alcohol reward concentration in % for the sipper

+ Door move time (s): time the guillotine door needs to get out of the way for the sipper to be accessible (_default_: **1.5** seconds)


__*Recorded Data:*__

+ total number of sipper lever presses (**A**), sampling trials included
+ total number of dipper lever presses (**B**), sampling trails included

+ timestamps (in seconds from experiment start) for each sipper lever press (**C**)
+ timestamps for each dipper lever press (**D**)

+ total number of sipper choices (**E**), sampling trials included
+ timestamps for the start of the sipper access times (**F**)

+ total number of dipper choices (**G**), sampling trials included
+ timestamps for the start of the dipper access times (**H**)
+ timestamps for the start of each trial (**J**)
+ timestamps for the end of each trial (**K**)

+ total number of licks (_only the sipper has a lickometer attached_) (**L**), sampling trials included
+ timestamps of each lick (**M**)

+ total sipper lever presses per trial (**P**), sampling trials included, i.e. per default it has 12 slots
+ total dipper lever presses per trial (**Q**), sampling trials included, i.e. per default it has 12 slots

+ the choice made during each trial (**R**), sampling trials included. This should have the same size as The
arrays P and Q that record the lever presses. The coding for the choices is as follows:
    - 0: no choice made, neither FR ratio was reached
    - 1: sipper chosen
    - 2: dipper chosen
    
+ fixed ratio for the sipper lever (**X**)
+ fixed ratio for the dipper lever (**Z**)

+ calculating the ratio for each phase (sampling or choice) (**Y**); the values will be reset when entering 
the choice phase and, since the choice phase is the last phase, recorded.
	- Y(0): count of sipper choices
	- Y(1): count of dipper choices
	- Y(2): ratio of sipper choices / (sipper choices + dipper choices)
	- Y(3): count of no choice trials
	


#### sipperCycle.mpc ####

This utility program cycles the sippers back and forth for a few times and is used in a macro at the beginning of the experimental day. This ensures that the sippers will travel the full distance of their slide every time. This program became necessary with the addition of the guillotine door and ensures that the sipper is not rammed into the door. 

The program cycles the sipper x number of times and stops with the sipper inserted into the chamber, so the operator can check each chamber and adjust the sipper position. All sippers are automatically retracted out of the chamber after the wait time is over.

__*Available Variables:*__

+ Number of cycles: (_default_: **5**)
+ Cycle interval (sec): (_default_: **1** second)
+ Start time offset (sec): (_default_: **0** seconds)
+ Wait time for input (min): (_default_: **10** minutes)

__*Recorded Data:*__

NONE


[1]: http://http://www.med-associates.com
[s1]: https://www.med-associates.com/?s=ENV-414
[s2]: https://www.med-associates.com/product/aversive-stimulatorscrambler-module/
[mc]: https://www.med-associates.com/product/classic-modular-test-chamber-with-modified-top-for-mouse/
[mcifp]: https://www.med-associates.com/product/smartctrl-4-input8-output-package/
[lixit]: https://www.med-associates.com/product/retractable-sipper-w-lixit-mouse-chamber/
[dip]: https://www.med-associates.com/product/switchable-liquid-dipper-for-classic-mouse-modular-chamber/
[p1]: https://currentprotocols.onlinelibrary.wiley.com/doi/full/10.1002/0471142301.ns0944s64
[iv]: https://bitbucket.org/r-bock/medpc-sa/src/master/IV%20self%20administration/
