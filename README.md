# Operant Self-Administration Programs #

This respository is a collection of behavior programs controlling operant behavior chambers by [Med Associates][1]. The programs are all written in the MedState Notation, used to control the operant chambers via the proprietary software Med-PC&reg; IV. The published programs have been tested and used with _Med-PC IV_. As of this update (_June 2022_) the latest software version is [Med-PC V][2]. All these programs have been successfully compiled on it and should work.

**Important Note:** this documentation, similar to the programs in this collection, is ongoing work, and as so often, the documentation usually lacks behind the program development versions. For further questions, please drop [me][5] an email. Please use these protocols at your own discretion, they come with NO WARRENTY AT ALL.

### What is this repository for? ###

This collection of programs are all used in self-administration operant tasks, that have been developed and evolved in the laboratory setting since 2009. Some of the programs are used less often and represent an older state. All currently used programs will have a version number stored within the data file, after I realized that some features added started to make older data files harder to analyze. The programs have been developed in conjunction with an [Igor Pro][3] based analysis system.

The programs are separated by the following sections based on their implemented tasks

+ [_IV self-administration_][6]  
	These programs control a operant box with 2 levers, cue lights and a syringe pump. The syringe pump is connected via a swivel to an intravenous catheter in the jugular vein of the mouse. Responding on the active lever results in the delivery of a predefined solution volume/drug dose through the catheter. 

+ [_sipper self-administration_][7]  
      These programs work control a mouse chamber with 2 levers and a retractable sipper. A lickometer is attached to the sipper to count the licks. Since the lickometer counted licks while the sipper was retracted from the chamber, i.e. the mouse still reached it, the sipper hole is now covered with a guillotine door, which requires an extra care of control to avoid the sipper slamming into a closed door.

+ [_self stimulation_][10]  
	These programs are the latest addition to this set and work with the operant boxes. The sipper and lickometer have been removed and TTL boxes added, which trigger light stimuli to the brain via optical fibers. A lever press on the active lever will activate a given train and stimulate the brain. LED lights, attached to a commutator mounted in roof of the attenuating box, provide the light pulses delivered via patch cord into the brain of the mouse.

+ [_food receptical_][8]  
	These programs work on the modified operant boxes used with the IV self-administration. The syringe pump is exchanged for a food receptical with a head entry detector and a pellet dispenser connected to it. The food receptical is mounted between the 2 levers. Most recently, some programs have been added to be used on modified sipper chambers.
		 
+ [_box checks_][9]  
	A set of programs useful for utility functions or operant chamber (box) maintainance.


### How do I get set up? ###

If you are using the git as your version control system, you will get the all programs in this repository. Either use your command line 

```
git clone https://r-bock@bitbucket.org/r-bock/medpc-sa.git
```

or use the free [SourceTree][4] desktop program to install the files. 

Once you have cloned the repository, copy the files you need to the MPC folder on your recording computer, which must have at least _Med-PC IV_ installed. 

Before using them you need to make sure that the port settings in the individual programs match your own boxes setup (or change your boxes to match the ports of the program). The port settings are listed after the description of the program. It is best to use the _Trans IV_ program that comes with Med-PC to adjust the program ports to your specific setup. When all programs have been adjusted and checked that the ports match they have to be compiled. The Med-PC IV program itself need to be closed for the compilation to work. Once they are compiled, the programs will show up in the "Open session" dialog in Med-PC IV.

_Note:_
These programs have all been successfully compiled with Med-PC V. Depending on the used 
operating system to download this repository, there might be a slew of error messages, which can be simply due to the fact, that the _Trans_ programs expect Windows style line endings. The files from the repository might give you Unix style line endings for the text files. If this happens, the easiest and quickest way to fix it is:

+ Open each program file with Notepad and copy the whole contents
+ Open the _Trans V_ program
+ Make a new file
+ Paste the copied code into the new file
+ Save the file in _Trans V_ with the original name
+ Repeat for each program

+ Compile all new programs


### What do the programs do? ###

Please see the individual folders for a more detailed description of the individual programs.

### Publications ###

These programs have been used for the data sets that went into the following scientific publications:

1. Bocarsly, ME, _et al._ (2019). **A Mechanism Linking Two Known Vulnerability Factors for Alcohol Abuse: Heightened Alcohol Stimulation and Low Striatal Dopamine D2 Receptors**. _Cell Reports_. doi: [10.1016/j.celrep.2019.09.059](https://doi.org/doi: 10.1016/j.celrep.2019.09.059), PMCID: [PMC6880649](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6880649/)
1. Dobbs LK, _et al._ (2019). **D1 receptor hypersensitivity in mice with low striatal D2 receptors facilitates select cocaine behaviors**. _Neuropsychopharm_. doi: [10.1038/s41386-018-0286-3](https://doi.org/10.1038/s41386-018-0286-3), PMCID: [PMC6372593](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6372593/)
2. Blegen MA, _et al._ (2017). **Alcohol operant self-administration: Investigating how alcohol seeking behaviors predicts drinking in mice using two operant approaches**. _Alcohol_. doi: [10.1016/j.alcohol.2017.08.008](https://doi.org/10.1016/j.alcohol.2017.08.008), PMCID: [PMC5939586](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5939586/)
3. Holroyd KB, _et al._ (2015). **Loss of feedback inhibition via D2 autoreceptors enhances acquisition of cocaine taking and reactivity to drug-paired cues**. _Neuropsychopharm_. doi: [10.1038/npp.2014.336](https://doi.org/10.1038/npp.2014.336), PMCID: [PMC4397408](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4397408/)
4. Bock R, _et al._ (2013). **Strengthening the accumbal indirect pathway promotes resilience to compulsive cocaine use**. _Nat Neuroscience_. doi: [10.1038/nn.3369](https://doi.org/10.1038/nn.3369), PMCID: [PMC3637872](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3637872/)

### Who do I talk to? ###

This set of programs was first added to the repository by [me][5] in 2016. Please feel free to drop me an email with any question that arises. 

[1]: http://www.med-associates.com/
[2]: https://www.med-associates.com/med-pc-v/
[3]: http://www.wavemetrics.com/
[4]: https://www.sourcetreeapp.com/
[5]: mailto:rolandbock@gmail.com?subject=[MedPC]:%20Repository%20Question
[6]: https://bitbucket.org/r-bock/medpc-sa/src/master/IV%20self%20administration/
[7]: https://bitbucket.org/r-bock/medpc-sa/src/master/sipper%20self%20administration/
[8]: https://bitbucket.org/r-bock/medpc-sa/src/master/food%20receptical/
[9]: https://bitbucket.org/r-bock/medpc-sa/src/master/box%20checks/
[10]: https://bitbucket.org/r-bock/medpc-sa/src/master/self%20stimulation/
