# Operant Self-Administration Programs for Food Delivery #

### What is this repository for? ###

This collection of programs are all used in food self-administration operant tasks. 

The initial programs (training, sessions and breakpoint) derive from earlier versions of the [IV self administration][iv] programs, so share common features. 

### How do I get set up? ###

Download the individual files and place them into the MPC folder inside your _Med-PC V_ setup on your computer that is connected to the chambers. Modify the port settings according to your own boxes setup (or change your boxes to match the ports of the program). The port settings are listed after the description of the program. It is best to use the _Trans V_ program that comes with _Med-PC_ to change and compile them. The programs need to be compiled before they can be used. The _Med-PC V_ program itself need to be closed for the compilation to work. Once they are compiled, the programs will show up in the "Open session" dialog in _Med-PC V_.

These programs have been written for _Med-PC IV_ and successfully compiled with _Med-PC V_. 


## Program Descriptions ##

### Hardware setup ###

The older versions of these programs have been written with the extra wide modular mouse chamber [ENV-307W][mcw] and the newer ones with standard modular mouse chamber [ENV-307A-CT][mca] in mind, but they should be working with both settings irrespectively. The standard chambers use the [SmartCtrl Package (4 in/8 out) DIG-716P1][mcifp] providing 4 inputs and 8 outputs, while the extra wide chambers are setup with the [SmartCtrl Package (8 in/16 out) DIG-716P2][mcifp2], are more likely to require adjustment to the port settings.

In _extra wide chambers_ the food receptical is mounted between the 2 levers and a cue/availability light is above each lever. The house light is mounted on the opposite wall. The _standard chambers_ have the receptical mounted on the opposite wall of the levers.

The programs starting with *food_wf* require the syringe pump to deliver the food solution. In this case a **6 ml** syringe is used instead of the 3 ml for the IV SA. 

### General definitions ##

When referring to the right or left lever, right or left is considered from the mouse perspective, when the mouse faces the wall where the levers are mounted. Most recorded events are stored as timestamps in an array. The timestamp is counted in seconds at 0.01 second resolution from the beginning of the experiment. 

### Self-administration programs ###

| file name | description |
| --------- | ----------- |
|[food_wf.mpc](#markdown-header-food_wfmpc) | main program for self-administration |
|food_wf_breakpoint.mpc | extinction program for the session |
|food_extinction.mpc | extinction program |
|food_wf_ext_n.mpc | extinction program |
|food_wf_punishment.mpc | food punishment (food shock) program |
|        |         |
|[food_s_acquisition.mpc](#markdown-header-food_s_acquisitionmpc) | food pellet full access session - acquisition |
|[food_s_DRinSession.mpc](#markdown-header-food_s_DRinSessionmpc) | program to create demand curves for food pellets |
   

#### food_wf.mpc ####

This program is derived from the [training.mpc][tr] program for the IV self-administration that later evolved into the [training_l_nf.mpc][tr2] program. This older version starts with providing a free reward with the beginning of the session to ensure that each subsequent reward is of the desired volumen/concentration.

It is possible to set out a TTL train for external stimulation, if the pulse length is equal or larger than 10 milliseconds.

**_Available Variables:_**

These variables below can be set through the `Named Variables` dialog in the MedPC program or through macros with a `SET ... VALUE ... MAINBOX ... BOX ...` statement.

+ Reward volume (ul) - volume of each reward in microliter (default: **10** µl)

+ Post reward time-out (sec) - time-out period after infusion (default: **0** seconds)
+ Session length (min) - the length of the total session in minutes (default: **120** minutes)
+ Poke ratio - the desired fixed ratio responding (default: **1**)
+ Active hole (7 for right or 8 for left) - set the active side (default: **7** for **right**)
+ Animal Weight (g) - weight of the mouse in grams (default: **25** g)
+ Reward concentration (%) - the concentration of the solution in the syringe (default: **10** %)
+ Pumping rate (ul/s) - the speed of the syringe pump (default: **12** µl/s)

+ TTL status - turn a TTL pulse/train on or of (default: **Off**)
+ TTL signal length (s) - length of the TTL signal (default: **0.01** seconds)
+ TTL signal interval (s) - length of the TTL signal interval in a train (default: **0.05** seconds)
+ TTL train length (s) - length of the total TTL train
+ TTL train interval (s) - interval of the TTL trains

  
**_Recorded Data:_**

_variables_

A
: count of the total _active_ responses (nose pokes or lever presses) - a single number.

B
: count of the total _inactive_ responses - a single number.

J
: count of the total _rewards_, similar to A and B

T
: elapsed time counter from the start of the experiment in 10 millisecond increments

X
: the fixed ratio set for this experiment

_arrays_

C
: array of the timestamps (in seconds since the start of the experiment) for _active_ responses.

D
: array of the timestamps for _inactive_ responses.

E
: array for error codes.

F
: array for error code time stamps.

H
: array for the active and inactive input ports .
: H(0) is the input port number for the _active_ lever or nose poke (default is **right**),
: H(1) is the input port number for the _inactive_ lever or nose poke (default is **left**),
: H(2) is the output port number for the _active_ cue light
: H(3) is the output port number for the _active_ nose poke hole - if it exist
: H(4) contains the version number of the program for the .sad file

K
: array for the _reward_ delivery timestamps (see also C and D)

S
: an array to hold various time variable for calculations

W
: an array to hold various animal related variables

#### food_s_acquisition.mpc ####

This program is modeled after the acquisition program in the IV self-administration section and the newest addition to this set. It implements a single, full access session that terminates either after the given time of the session length or after reaching the maximum number of rewards, depending on what occurs first. 

Lever presses or pokes occurring during the reward delivery period or a post reward time-out period do not count towards the next ratio. A cue light mounted above the active side signals reward availability and will turn off when lever presses have no effect.

All lever presses on both sides and the head entries will be continuously recorded over the course of the session with a timestamp in seconds counting from the start.

__*Available Variables:*__

+ total session length in minutes (_default_: **120 minutes**)
+ maximum number of rewards (_default_: **20**)
+ fixed ratio (_default_: **1**)
+ side of the active lever (_default_: **right (2)**; the number is the port number of the lever)
+ post-reward time-out (_default_: **0** seconds) 

+ the mouse weight
+ the reward size (_default_: **20** mg pellets)

__*Recorded Data:*__

+ number of active and inactive lever presses and licks
+ timestamps (in 0.01 seconds resolution from the start of the experiment) for the active
and inactive presses and the licks
+ head entry events for the receptical
+ weight of the mouse and size of the reward

_variables_

A
: counter for active responses

B
: counter for inactive responses

E
: counter for head entries

J
: counter for delivered rewards

T
: elapsed time in seconds from the start of the experiment

X
: fixed ratio of responses

_arrays_

C
: timestamps for active responses (max. 10,000)

D
: timestamps for inactive responses (max. 10,000)

E
: timestamps for the head entries (max. 30,000)

K
: timestamps for the delivered rewards (max. 10,000)

S
: store various timing variables

W
: store various animal related variables


#### food_s_DRinSession.mpc ####

This program creates the data for demand curves for the motivation analysis in the context of behavior economy. It increases the ratio in up to 10 access periods, separated by a no-access period. In the most simple sense, it creates data for dose-response curve within the session.  

The number of access sessions, the length of the access sessions and their pause interval length together determine the total session length. The no-access pause intervals are indicated to the mouse by turning the house light on and the cue light above the active lever off. The access periods/bins happen in darkness (no house light), with the cue light above the active lever turned on. All lever presses on both sides and the head entries will be continuously recorded over the course of the session with a timestamp in seconds counting from the start. In addition, the lever presses, rewards and head entries will be counted per phase/bin and displayed on the screen.

The ratios for up to 10 access periods can be set and will be used in sequential order - as set.

As with all other programs, setting the active lever, will also automatically sets the active cue light.


__*Available Variables:*__

+ number of active sessions (_default_: **5**)
+ active period length (min) (_default_: **5** min)
+ pause period length (min) (_default_: **20** min)

+ Ratio for period 1...10 (_default_ sequence: **1**, **3**, **6**, **10**, **20**, **35**,  **70**, **120**, **210**, **360**)

+ side of the active lever (_default_: **right (2)**; the port number of the active lever)
+ weight of the mouse in g
+ reward size (pellet size) (_default_: **20** mg pellets)


__*Recorded Data:*__

+ number of lever presses, rewards and head entries, binned per period (access and no-access)
+ timestamps (in 0.01 seconds resolution from the beginning of the experiment/session) for lever presses, rewards and head entries.
+ weight of the mouse
+ size of the reward


_variables_

T
: elapsed time counter from the start of the experiment in 10 millisecond increments

X
: the fixed ratio for the various active bins in this experiment, holds the last used ratio at the end of the experiment

_arrays_

A
: count of the total _active_ responses (nose pokes or lever presses) - per bin.

B
: count of the total _inactive_ responses - per bin.

C
: timestamp array for the _active_ presses (max. 10,000)

D
: timestamp array for the _inactive_ presses (max. 10,000)

E
: count of the total _head entries_ - per bin.

F
: timestamp array for the _head entries_ (max. 30,000)


H
: array to hold the port numbers for the levers and cue lights; last number is the version number

I
: array for various indices into the other arrays

J
: count of the _rewards_ - per bin

K
: timestamp array for the _rewards_ (max. 10,000)

P
: array storing the start and end timestamps for the active periods/bins. Helps to visiualize the data in graphs and serves as a control for the correct period lengths.

S
: array storing various timing variables for the program to run. S(0) holds the number of active periods, S(1) the length of the active period in minutes and S(2) the lengths of the interval between the active periods in minutes.

W
: stores some animal related information like the weight and the pellet size.

Y
: stores the ratio sequence for the active periods. Maximal 10 are possible.




[iv]: https://bitbucket.org/r-bock/medpc-sa/src/master/IV%20self%20administration/
[mcw]: https://www.med-associates.com/product/extra-wide-modular-test-chamber-for-mouse/
[mca]: https://www.med-associates.com/product/classic-modular-test-chamber-with-modified-top-for-mouse/
[mcifp]: https://www.med-associates.com/product/smartctrl-4-input8-output-package/
[micfp2]: https://www.med-associates.com/product/smartctrl-8-input16-output-package/
[tr]: https://bitbucket.org/r-bock/medpc-sa/src/master/IV%20self%20administration/chamber%20set%201/training.mpc
