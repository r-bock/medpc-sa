# Operant Self-Administration Programs for Optical Self Stimulation #

### What is this repository for? ###

This collection of programs that are used for operant self-stimulation of rodents with optical pulses.

### How do I get set up? ###

Download the individual files and place them into the MPC folder inside your _Med-PC IV_ setup on your computer that is connected to the chambers. Modify the port settings according to your own boxes setup (or change your boxes to match the ports of the program). The port settings are listed after the description of the program. It is best to use the _Trans IV_ program that comes with _Med-PC_ to change and compile them. The programs need to be compiled before they can be used. The _Med-PC IV_ program itself need to be closed for the compilation to work. Once they are compiled, the programs will show up in the "Open session" dialog in _Med-PC IV_. 

## Program Descriptions ##

### Hardware setup ###

The programs have been written with the standard modular mouse chamber [ENV-307A-CT][mc] in mind using the [SmartCtrl Package (4 in/8) DIG-716P1][miscp4] or [SmartCtrl Package (8 in/16 out) DIG-716P2][miscp8]. One wall has 2 retractable levers with a cue light each mounted above them. Per default the cue lights are disabled (off), but present in the box. The correct response on the active lever results in a train of TTL pulses. The TTL box is connected to an LED driver that powers and controls the LEDs mounted on a commutator on the roof of the sound attenuating box. 

### General definitions ##

The levers are mounted on each side of the sipper, so when referring to the right or left lever, right or left is considered from the mouse perspective, when the mouse faces the sipper. Most recorded events are stored as timestamps in an array. The timestamp is counted in seconds at 0.01 second resolution from the beginning of the experiment. 

### Self-stimulation programs ###

| file name | description |
| --------- | ----------- |
|ost_training_is.mpc | training program for self-stimulation with the stimulation controlled by MedPC |
|ost_doublestim_is.mpc| derived from the training program, but with the option to use 2 light sources |
|ost_session_is.mpc | main program for self-stimulation with the stimulation controlled by MedPC |
|ost_session.mpc | main program for self-stimulation |


#### ost_training_is.mpc ####

This program has been designed for the training phase for the animals. It consists of a single, continuous access session. 
The activation of the active lever results in a defined trigger train to a LED, the activation of the inactive control lever 
has no effect. The stimulation train consists of 1 to many bursts of individual TTL pulses that activate the stimulation light
source.

__*Available Variables:*__

+ total session length in minutes (_default_: **120 minutes**)
+ pulse length of the TTL pulse (_default_: 10 ms **0.01 seconds**)
+ pulse frequency (_default_: **20 Hz** 50 ms interval)
+ number of pulses (_default_: **5**)
+ train frequency (_default_: **2 Hz** 500 ms interval)
+ number of trains (_default_: **8**)
+ side of the active lever (_default_: **right (2)**; the number is the port number of the lever)
+ the mouse weight (_not strictly needed for the program to work, but nice to have recorded here_)


__*Recorded Data:*__

+ number of active and inactive lever presses and licks
+ timestamps (in 0.1 seconds resolution from the start of the experiment) for the active
and inactive presses and the licks
+ timestamps of the delivered pulses
+ weight of the mouse and concentration of the reward solution


#### ost_doublestim_is.mpc ####

This program is directly derived from the training program "ost_training_is.mpc" and adds a second light source for the optical
stimulation. The second light source can be separately activated. 

#### ost_session_is.mpc ####

This program is the main program for the suite of the sipper operant chamber programs. It provides a continues access session. One of the levers can be assigned as the active one, the other will automatically be set as the inactive one. Independent of the lever assignment, the active one will always record the data into the "active" array and the same for the inactive. (This program is currently identical to the __ost_session.mpc__ program which will be removed in a future release.)

__*Available Variables:*__

+ total session length in minutes (_default_: **120 minutes**)
+ sipper access time in seconds (_default_: **10 seconds**)
+ the desired random ratio (_default_: **2**, i.e. every second press should gain access time)
+ side of the active lever (_default_: **right (2)**; the number is the port number of the lever)

+ the mouse weight (_not strictly needed for the program to work, but nice to have recorded here_)


__*Recorded Data:*__

+ number of active and inactive lever presses and licks
+ timestamps (in 0.1 seconds resolution from the start of the experiment) for the active
and inactive presses and the licks
+ start of each sipper accesstime (as timestamp) and their duration
+ weight of the mouse and concentration of the reward solution



[1]: http://http://www.med-associates.com
[miscp4]: https://www.med-associates.com/product/smartctrl-4-input8-output-package/
[miscp8]: https://www.med-associates.com/product/smartctrl-8-input16-output-package/
